<?php

namespace App\Helpers;

if (!ini_get("auto_detect_line_endings")) {
    ini_set("auto_detect_line_endings", '1');
}
use League\Csv\Reader;

class Importer
{

    public static function returnObjectData($path, $header, $account_id = null)
    {
        $reader = Reader::createFrompath($path);
        $headers = $reader->fetchOne();
        $dataSet = [];
        $index = 0;

        $reader->setOffset(1);

        $data = $reader->fetchAll();

        foreach ($data as $d => $v) {
            $return[$d] = [
                'last_name' => $v[$header['last_name']],
                'first_name' => $v[$header['first_name']],
                'number' => $v[$header['number']],
            ];
        }
        return $return;
    }

    public static function getHeaders($path)
    {
        $reader = Reader::createFrompath($path);

        return $reader->fetchOne();
    }

    public static function sanitizeNumber($number)
    {
        $number = str_replace('+63', '0', $number);
        $number = trim($number);
        $number = str_replace(' ', '', $number);
        $number = str_replace('-', '', $number);
        return $number;
    }
}
