<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'number',
        'user_id'
    ];

    protected $casts = [
        'id' => 'integer'
    ];

    protected $appends = [
        'message_count',
    ];

    public function directories()
    {
        return $this->belongsToMany('App\Directory');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function messages()
    {
        return $this->hasMany('App\Message')->orderBy('id', 'desc');
    }

    public function getMessageCountAttribute()
    {
        return $this->messages()->count();
    }
}
