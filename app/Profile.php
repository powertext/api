<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'header',
        'first_name',
        'last_name',
        'email',
        'phone',
        'user_id'
    ];

    protected $casts = [
        'id' => 'integer'
    ];

    public function campaigns()
    {
        return $this->hasMany('App\Campaign');
    }

    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }

    public function directories()
    {
        return $this->hasMany('App\Directory');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
