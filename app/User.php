<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'semaphore_key'];

    protected $casts = [
        'id' => 'integer'
    ];

    protected $appends = [
        'message_count'
    ];

    public function getMessageCountAttribute()
    {
        return $this->messages()->count();
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }

    public function campaigns()
    {
        return $this->hasMany('App\Campaign');
    }

    public function directories()
    {
        return $this->hasMany('App\Directory');
    }

    public function purchases()
    {
        return $this->hasMany('App\Purchase');
    }

    public function credits()
    {
        return $this->hasMany('App\Credit');
    }

    public function messages()
    {
        return $this->hasMany('App\Message')->orderBy('id', 'desc');
    }
}
