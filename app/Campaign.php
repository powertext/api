<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = [
        'name',
        'message',
        'header',
        'is_sent',
        'account_id',
    ];

    protected $casts = [
        'is_sent' => 'boolean',
        'id' => 'integer'
    ];

    public function contacts()
    {
        return $this->belongsToMany('App\Contact')->withPivot('status');
    }

    public function directories()
    {
        return $this->belongsToMany('App\Directory');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }
}
