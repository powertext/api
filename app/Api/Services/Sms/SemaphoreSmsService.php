<?php

namespace App\Api\Services\Sms;

use App\Api\Services\SmsService;
use App\Exceptions\Semaphore\FeatureNotAllowed;
use App\Exceptions\Semaphore\GatewayDownException;
use App\Exceptions\Semaphore\InvalidOptionsException;
use App\Exceptions\Semaphore\NotAuthorizedException;
use App\Exceptions\Semaphore\NotEnoughBalanceException;

class SemaphoreSmsService implements SmsService
{
    /**
     *
     * @param  string $number
     * @param  string $message
     * @return boolean
     */
    public function send($number, $message, $from = 'POWERTEXT')
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "http://api.semaphore.co/api/sms");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query([
                'api' => config('SEMAPHORE_KEY', 'xYrni3tq4zswyJCkWsZx'),
                'number' => $number,
                'message' => $message,
                'from' => $from,
            ]));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = json_decode(curl_exec($ch));

        curl_close($ch);

        switch ($server_output->code) {
            case 105:
                break;
            case 104:
                throw new GatewayDownException($server_output->status);
                break;
            case 103:
                throw new InvalidOptionsException($server_output->status);
                break;
            case 102:
                throw new FeatureNotAllowed($server_output->status);
                break;
            case 101:
                throw new NotEnoughBalanceException($server_output->status);
                break;
            case 100:
                throw new NotAuthorizedException($server_output->status);
                break;
            default:
                return true;
                break;
        }
    }

    /**
     * @param  array  $numbers
     * @param  string $message
     * @return boolean
     */
    public function sendMany(array $numbers, $message, $from = 'POWERTEXT')
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "http://api.semaphore.co/api/sms");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query([
                'api' => config('SEMAPHORE_KEY', 'xYrni3tq4zswyJCkWsZx'),
                'number' => implode(',', $numbers),
                'message' => $message,
                'from' => $from,
            ]));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = json_decode(curl_exec($ch));

        curl_close($ch);

        switch ($server_output->code) {
            case 104:
                throw new GatewayDownException($server_output->status);
                break;
            case 103:
                throw new InvalidOptionsException($server_output->status);
                break;
            case 102:
                throw new FeatureNotAllowed($server_output->status);
                break;
            case 101:
                throw new NotEnoughBalanceException($server_output->status);
                break;
            case 100:
                throw new NotAuthorizedException($server_output->status);
                break;
            default:
                return true;
                break;
        }
    }

    /**
     * @return int
     *
     */
    public function getCredits($semaphoreKey)
    {
        $data = file_get_contents("http://api.semaphore.co/api/sms/account?api=" . $semaphoreKey);

       return json_decode($data);     
      
    }
}
