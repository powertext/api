<?php

namespace App\Api\Services;

interface SmsService
{
	/**
	 *
	 * @param  string $number
	 * @param  string $message
	 * @return boolean
	 */
	public function send($number, $message, $from = 'POWERTEXT');

	/**
 * @param  array  $numbers
	 * @param  string $message
	 * @return boolean
	 */
	public function sendMany(array $numbers, $message, $from = 'POWERTEXT');

    /**
     * @param string semaphore_key
     * @return int
     */
    public function getCredits($semaphoreKey);

}
