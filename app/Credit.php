<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{

	protected $fillable = ['type', 'user_id', 'reference_id', 'amount'];

    protected $casts = [
        'id' => 'integer'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function reference()
    {
        return $this->morphTo();
    }

    public function scopeEarned($query)
    {
    	return $query->where('type', 'earned');
    }

    public function scopeSpent($query)
    {
    	return $query->where('type', 'spent');
    }
}
