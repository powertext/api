<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	protected $guarded = ['user_id'];

    protected $casts = [
        'id' => 'integer'
    ];

    public function credit()
    {
    	return $this->morphMany('App\Credit', 'reference');
    }

    public function scopeFailed($query)
    {
    	return $query->where('status', 'failed');
    }

    public function scopeDelivered($query)
    {
    	return $query->where('status', 'delivered');
    }
}
