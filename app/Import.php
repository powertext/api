<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
	/**
	 * @var array
	 */
    protected $guarded = [];

    protected $casts = [
    	'columns' => 'array',
        'id' => 'integer'
    ];

    public function directory()
    {
    	return $this->belongsTo('App\Directory');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
