<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	protected $fillable = ['amount' , 'credits'];

	protected $casts = [
        'id' => 'integer'
    ];

    public function credit()
    {
    	return $this->morphMany('App\Credit', 'reference');
    }
}
