<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Tymon\JWTAuth\Exceptions\JWTException;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();
        
            App::singleton('user', function () use ($user) {
                return $user;
            });

            config([
                'SEMAPHORE_KEY' => $user->semaphore_key
            ]);
        } catch (JWTException $exs) {
           
            return response()->json([
                'message' => 'unauthorized',
                'data' => [
                  'message' => 'unauthorized',
                  'error' => 'User is not authenticated or Header is missing'
                  ] 
             ], 401);
    	}
	
	   // $user = \App\User::find(1);

     //   App::singleton('user', function () use ($user) {
     //       return $user;
     //   });

        return $next($request);
    }
}
