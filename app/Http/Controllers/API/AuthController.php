<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * @param  \App\Http\Requests\AddUserRequest $request
     * @return Response
     */
    public function register(\App\Http\Requests\AddUserRequest $request)
    {
       
        $u = new \App\User;

        $u->email = $request->get('email');
        $u->password = \Hash::make($request->get('password'));
        $u->name = $request->get('first_name') . ' ' . $request->get('last_name');

        $u->save();

        $profile = new \App\Profile;
        $profile->header = $request->get('header');
        $profile->first_name = $request->get('first_name');
        $profile->last_name = $request->get('last_name');
        $profile->phone = $request->get('phone');

        $u->profile()->save($profile);

        return response()->json([
            'message' => 'created',
            'token' => \JWTAuth::fromUser($u),
            'data' => $u->with('profile')->get()
            ], 201);
    }

    public function authenticate(\App\Http\Requests\AuthenticateRequest $request)
    {

//        return $request->input();

        $user = \Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')]);

        if($user) {
            $token = \JWTAuth::fromUser(\Auth::user());
            return response([
                'message' => 'authentication successful',
                'token' => $token
            ]);
        } else {
            return response([
                'message' => 'wrong username  / password',
                'status' => 'error'
            ], 401);
        }
    }
}
