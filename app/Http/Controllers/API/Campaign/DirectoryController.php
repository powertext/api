<?php

namespace App\Http\Controllers\API\Campaign;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DirectoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(\App\Campaign $campaign, Request $request)
    {
        return $campaign->directories()->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(\App\Campaign $campaign, Request $request)
    {
        $directory = \App\Directory::find($request->get('directory_id'));

        $campaign->contacts()->attach($directory->lists('id'));

        return response()->json([
            'message' => 'attached'
            ]);
    }
}
