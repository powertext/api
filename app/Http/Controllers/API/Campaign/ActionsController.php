<?php

namespace App\Http\Controllers\API\Campaign;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Api\Services\SmsService;
use App\Directory;

class ActionsController extends Controller
{
    /**
     * @param  \App\Campaign $campaign
     * @param  Request       $request
     * @return Response
     */
    public function sendCampaign(\App\Campaign $campaign, SmsService $smsService, Request $request)
    {
        // if($campaign->is_sent) {
        //     return response()->json(['message' => 'campaign already sent'], 400);
        // }

        $user = app('user');

        foreach($campaign->contacts as $contact) {
            $smsService->send($contact->number, $campaign->message, $user->profile->header);

            $message = new \App\Message([
                'contact_id'    => $contact->id,
                'campaign_id'   => $campaign->id,
                'status'        => 'delivered',
                'message'       => $campaign->message
                ]);

        //    $user = app('user');

            $user->messages()->save($message);

            $campaign->contacts()->detach($contact->id);
            $campaign->contacts()->attach($contact->id, ['status' => 'delivered']);
        }

        $campaign->is_sent = true;

        $campaign->save();

    return response()->json([
            'message' => 'message sent',
            'sent' => $campaign->contacts()->where('status', 'delivered')->count(),
            'data' => $campaign->toArray()
            ]);
    }
}
