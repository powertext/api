<?php

namespace App\Http\Controllers\API\Campaign;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(\App\Campaign $campaign, Request $request)
    {
        return $campaign->contacts()->paginate($request->get('limit'));
    }

    public function store(\App\Campaign $campaign, \App\Http\Requests\AddCampaignContactsRequest $request)
    {
        $contacts = \App\Contact::whereIn('id', json_decode($request->get('contacts'), true))->get();
        
        $campaign->contacts()->attach($contacts->lists('id')->toArray());

        return response()->json(['message' => 'contacts attached',
            'data' => $contacts]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(\App\Campaign $campaign, Request $request)
    {
        $campaign->contacts()->detach(json_decode($request->get('contacts'), true));

        return response()->json([
            'message' => 'contacts removed'
            ]);
    }
}
