<?php

namespace App\Http\Controllers\API\Campaign;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(\App\Campaign $campaign, Request $request)
    {
        return $campaign->messages()->paginate($request->get('limit'));
    }
}
