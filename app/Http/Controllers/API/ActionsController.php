<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Api\Services\SmsService;
use App\Contact;
use App\Http\Requests\SendSmsRequest;
use App\Directory;

class ActionsController extends Controller
{
	/**
	 * @param  Contact        $contact
	 * @param  SendSmsRequest $request
	 * @param  SmsService     $smsService
	 * @return Reponse
	 */
    public function send(Contact $contact, /*SendSmsRequest $request,*/ SmsService $smsService)
    {
        $user = app('user');

    	$smsService->send($contact->number, $request->get('message'), $user->profile->header);

        $message = new \App\Message([
            'contact_id' => $contact->id,
            'status' => 'delivered',
            'message' => $request->get('message')
            ]);

        $user->messages()->save($message);

    	return response()->json([
    		'message' => 'message sent'
    		], 200);
    }

    /**
     * @param  Directory      $directory
     * @param  SendSmsRequest $request
     * @param  SmsService     $smsService
     * @return Response
     */
    public function sendToDirectory(Directory $directory, SendSmsRequest $request, SmsService $smsService)
    {
        $user = app('user');

        foreach($directory->contacts as $contact) {
            $smsService->send($contact->number, $request->get('message'), $user->profile->header);

            $message = new \App\Message([
                'contact_id' => $contact->id,
                'status' => 'delivered',
                'message' => $request->get('message')
                ]);

            $user->messages()->save($message);
        }

    	return response()->json([
    		'message' => 'message sent'
    		], 200);
    }

    /**
     * @param  \App\Campaign $campaign
     * @param  Request       $request
     * @return Response
     */
    public function sendCampaign(\App\Campaign $campaign, SmsService $smsService, Request $request)
    {
        $user = app('user');

        foreach($campaign->contacts as $contact) {
            $smsService->send($contact->number, $campaign->message, $user->profile->header);

            $campaign->contacts()->detach($contact->id);
            $campaign->contacts()->attach($contact->id, ['status' => 'delivered']);
        }

        $campaign->is_sent = true;
        $campaign->save();

    return response()->json([
            'message' => 'message sent',
            'data' => $campaign->toArray(),
            'sent' => $campaign->contacts()->where('status', 'delivered')->count()
            ]);
    }
}
