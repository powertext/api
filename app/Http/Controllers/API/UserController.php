<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function me()
    {
        $user = app('user');
       
        $user =  array_merge($user->with('profile')->where('id', $user->id)->first()->toArray(), [
            'available_credits' => $user->credits()->earned()->sum('amount') - $user->credits()->spent()->sum('amount')
    ]);
	return response()->json([
		'data' => $user
	]);
    }

    public function stats()
    {
        $user = app('user');

        $purchased = $user->credits()->earned()->sum('amount');
        $spent = $user->credits()->spent()->sum('amount');

        return response()->json([
            'available_credits' => (int) $purchased - $spent,
            'total_purchased'   => (int) $purchased,
            'credits_consumed'  => (int) $spent,
            'total_sms_sent'    => (int) $user->messages()->delivered()->count(),
            'total_sms_failed'  => (int) $user->messages()->failed()->count(),
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return \App\User::with('profile')->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(\App\Http\Requests\AddUserRequest $request)
    {
        $u = new \App\User;

        $u->email = $request->get('email');
        $u->password = \Hash::make($request->get('password'));
        $u->name = $request->get('first_name') . ' ' . $request->get('last_name');

        $u->save();

        $profile = new \App\Profile;
        $profile->header = $request->get('header');
        $profile->first_name = $request->get('first_name');
        $profile->last_name = $request->get('last_name');
        $profile->phone = $request->get('phone');

        $u->profile()->save($profile);

        return response()->json([
            'message' => 'created',
            'data' => $u->with('profile')->get()
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(\App\User $user)
    {
        return $user->with('profile')->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(\App\User $user, \App\Http\Requests\AddUserRequest $request)
    {
        $u = $user;

        $u->email = $request->get('email');
        $u->password = \Hash::make($request->get('password'));
        $u->name = $request->get('first_name') . ' ' . $request->get('last_name');

        $u->save();

        $profile = $user->profile;
        $profile->header = $request->get('header');
        $profile->first_name = $request->get('first_name');
        $profile->last_name = $request->get('last_name');
        $table->phone = $request->get('phone');

        $u->profile()->save($profile);

        return response()->json([
            'message' => 'updated',
            'data' => $u->with('profile')->toArray()
            ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(\App\User $user)
    {
        $user->profile->delete();

        $user->delete();

        return response()->json([
            'message' => 'deleted'
            ], 200);
    }
}
