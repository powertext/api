<?php

namespace App\Http\Controllers\API\Directory;

use App\Helpers\Importer;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    /**
     * @param  \App\Directory $directory
     * @param  Request        $request
     * @return Response
     */
    public function index(\App\Directory $directory, Request $request)
    {
        return $directory->imports()->paginate($request->get('limit'));
    }

    /**
     * @param  \App\Directory       $directory
     * @param  Request              $request
     * @param  \Faker\Provider\Uuid $uuid
     * @return Response
     */
    public function store(\App\Directory $directory, \App\Http\Requests\AddImportRequest $request)
    {
        $filename = date('U') . '.csv';

        $request->file('file')->move(storage_path() . '/csvs', $filename);

        $headers = Importer::getHeaders(storage_path() . '/csvs/' . $filename);

        $import = new \App\Import([
            'filename' => $filename,
            'directory_id' => $directory->id,
            'columns' => $headers,
        ]);

        $import->save();

        return response()->json([
            'message' => 'csv uploaded',
            'data' => $import,
        ]);
    }

    /**
     * @param  \App\Directory $directory
     * @param  \App\Import    $import
     * @param  Request        $request
     * @return Response
     */
    public function import(\App\Directory $directory, \App\Import $import, Request $request)
    {
        $data = \App\Helpers\Importer::returnObjectData(storage_path() . '/csvs/' . $import->filename, $request->only(['first_name', 'last_name', 'number']));

        $successful = 0;
        $failed = 0;

        foreach ($data as $contact) {
            if (!(isset($contact['number']) || isset($contact['first_name']) || isset($contact['last_name']))) {
                ++$failed;
                continue;

            }

            $contact['number'] = Importer::sanitizeNumber($contact['number']);

            $c = \App\Contact::firstOrNew([
                'user_id' => app('user')->id,
                'number' => $contact['number'],
            ]);
            $c->first_name = $contact['first_name'];
            $c->last_name = $contact['last_name'];

            $c->save();
            //TODO: Do I need to detach everytime? Is this real life? Or is this just fantasy?
            $directory->contacts()->detach($c->id);
            $directory->contacts()->attach($c->id);

            ++$successful;
        }

        $import->is_imported = true;
        $import->save();

        return response()->json([
            'message' => 'processed',
            'data' => [
                'successful' => $successful,
                'failed' => $failed,
            ],
        ]);
    }

    /**
     * @param  \App\Directory $directory
     * @param  \App\Import    $import
     * @param  Request        $request
     * @return Response
     */
    public function destroy(\App\Directory $directory, \App\Import $import, Request $request)
    {
        unlink(storage_path() . '/csvs/' . $import->filename);

        $import->delete();

        return response()->json([
            'message' => 'import deleted',
        ]);
    }
}
