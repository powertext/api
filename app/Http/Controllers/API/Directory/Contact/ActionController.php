<?php

namespace App\Http\Controllers\API\DIrectory\Contact;

use App\Helpers\Importer;
use App\Http\Controllers\Controller;
use App\Repositories\ContactRepository as Contact;
use App\Repositories\DirectoryRepository as Directory;
use Illuminate\Http\Request;

class ActionController extends Controller
{
    private $directory;
    private $contact;

    public function __construct(Directory $directory, Contact $contact)
    {
        $this->directory = $directory;
        $this->contact = $contact;
    }

    public function postImport(Request $request, $id, $directory_id)
    {

        $data = $request->input('headers');
        $failed = 0;

        if ($request->hasFile('file') && $data) {

            $request->file('file')->move(storage_path() . '/csvs', date('U'));

            $toSave = Importer::returnObjectData(storage_path() . '/csvs/' . date('U'), $data, $id);
            foreach ($toSave as $details) {
                try {
                    $contacts = $this->contact->create($details);
                    $contacts->directories()->attach($directory_id);
                } catch (Exception $exs) {
                    $failed++;
                }
            }

            $this->result['message'] = 'Successfully imported all CSV files';
            $this->result['data'] = [
                'length' => count($toSave),
                'successes' => count($toSave) - $failed,
                'failed' => $failed,
            ];

        } else {

            $this->result['message'] = 'No Import found';
            $this->result['status'] = 200;

        }

        return response($this->result, $this->result['status']);

    }

    public function postCloneAll(Request $request, $id, $directory_id)
    {
        $input = $request->input();

        $originalDirectory = $this->directory->find($directory_id);

        foreach ($originalDirectory->contacts as $c) {
            $c->directories()->attach($input['directory_id']);
        }

        $this->result['message'] = 'Cloned all contacts';
        $this->result['status'] = 200;

        return response($this->result, $this->result['status']);

    }
}
