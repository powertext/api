<?php

namespace App\Http\Controllers\API\Directory;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(\App\Directory $directory, Request $request)
    {
        return $directory->contacts()->paginate($request->get('limit'));
    }

    /**
     * @param  \App\Directory $directory
     * @param  Request        $request
     * @return Response
     */
    public function store(\App\Directory $directory, Request $request)
    {
        $contact = new \App\Contact([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'number' => $request->get('number')
            ]);

        $contact->save();

        $directory->contacts()->attach($contact->id);

        return response()->json([
            'message' => 'attached',
            'data' => $contact
            ]);
    }
}
