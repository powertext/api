<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user = app('user');

        return $user->purchases()->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(\App\Http\Requests\AddPurchaseRequest $request)
    {
        $user = app('user');

        $purchase = new \App\Purchase([
            'amount' => $request->get('amount'),
            'credits' => $request->get('credits')
            ]);

        $user->purchases()->save($purchase);

        //get sum credits
        
        $credit = new \App\Credit([
            'type' => 'earned',
            'reference_id' => $purchase->id,
            'amount' => $purchase->amount
            ]);

        $user->credits()->save($credit);

        return response()->json([
            'message' => 'purchased',
            'data' => $purchase
            ]);
    }
}
