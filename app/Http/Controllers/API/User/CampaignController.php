<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user = app('user');
        return $user->campaigns()->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(\App\Http\Requests\AddCampaignRequest $request)
    {
        $user = app('user');
        $campaign = new \App\Campaign;
        $campaign->message = $request->get('message');
        $campaign->name = $request->get('name');
        $campaign->save();

        $contacts = $request->get('contacts');

        $directories = \App\Directory::whereIn('id', $request->get('directories'))->get();

        foreach($directories as $directory) {
            $contacts = array_merge($contacts, $directory->contacts()->lists('id'));
        }

        $campaign->contacts()->attach($contacts);

        return response()->json([
            'message' => 'created',
            'data' => $campaign->toArray()
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(\App\Campaign $campaign)
    {
        $user = app('user');
        return $campaign->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(\App\Campaign $campaign, \App\Http\Requests\UpdateCampaignRequest $request)
    {
        $user = app('user');
        $campaign->message = $request->get('message');
        $campaign->save();

        $conctacts = json_decode($request->get('contacts'), true);

        $directories = \App\Directory::whereIn('id', json_decode($request->get('directories'), true))->get();

        foreach($directories as $directory) {
            $contacts = array_merge($contacts, $directory->contacts()->list('id'));
        }

        $campaign->contacts()->attach($contacts);

        return response()->json([
            'message' => 'updated',
            'data' => $campaign->toArray()
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(\App\Campaign $campaign)
    {
        $user = app('user');
        $campaign->delete();

        return response()->json([
            'message' => 'deleted'
            ]);
    }

    public function duplicate(\App\Campaign $campaign, AddCampaignRequest $request)
    {
        $c = new \App\Campaign([
            'name' => $request->get('name'),
            'message' => $request->get('message')
            ]);

        $c->save();

        $c->contacts()->attach($campaign->contacts()->lists('id'));

        return response([
            'message' => 'created',
            'data' => $c->toArray()
            ]);
    }
}
