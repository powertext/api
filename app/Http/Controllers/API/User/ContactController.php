<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user = app('user');
        return $user->contacts()->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(\App\Http\Requests\AddContactRequest $request)
    {
        $user = app('user');
        $contact = new \App\Contact([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'number' => $request->get('number')
            ]);

        $contact = $user->contacts()->save($contact);

        return response()->json([
            'message' => 'created',
            'data' => $contact
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(\App\Contact $contact)
    {
        $user = app('user');
        return $contact->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(\App\Contact $contact, \App\Http\Requests\UpdateCampaignRequest $request)
    {
        $user = app('user');
        $contact->number = $request->get('number');
        $contact->first_name = $request->get('first_name');
        $contact->last_name = $request->get('last_name');
        $contact->save();

        return response()->json([
            'data' => $contact->toArray(),
            'message' => 'updated'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(\App\Contact $contact)
    {
        $user = app('user');
        $contact->delete();

        return response()->json([
            'message' => 'deleted'
            ]);
    }
}
