<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DirectoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user = app('user');
        // needs refactor
        // check mo to jc, sinama ko muna contacts for the mean time
        return $user->directories()->with('contacts')->paginate($request->get('limit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(\App\Http\Requests\AddDirectoryRequest $request)
    {
        $user = app('user');
        $directory = new \App\Directory;

        $directory->name = $request->get('name');

        $user->directories()->save($directory);

        return response()->json([
            'message' => 'created',
            'data' => $directory
            ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(\App\Directory $directory)
    {
        $user = app('user');
        return $directory->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(\App\Directory $directory, \App\Http\Requests\AddDirectoryRequest $request)
    {
        $user = app('user');
        $directory->name = $request->get('name');

        return response()->json([
            'message' => 'updated',
            'data' => $directory
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(\App\Directory $directory)
    {
        $user = app('user');
        $directory->delete();

        return response()->json([
            'message' => 'deleted'
            ]);
    }
}
