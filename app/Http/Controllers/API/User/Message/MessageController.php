<?php

namespace App\Http\Controllers\API\User\Message;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user = app('user');
        return $user->messages()->paginate($request->get('limit'));
    }
}
