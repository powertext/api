<?php

namespace App\Http\Controllers\API\Contact;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{

	/**
	 * @param  \App\Contact $contact
	 * @param  Request      $request
	 * @return Response
	 */
    public function index(\App\Contact $contact, Request $request)
    {
    	return $contact->messages()->paginate($request->get('limit'));
    }
}
