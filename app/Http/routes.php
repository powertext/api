<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('php', function () {
    phpinfo();
});

Route::group(['namespace' => 'API', 'prefix' => 'api'], function () {
    post('auth', 'AuthController@authenticate');
    post('register', 'AuthController@register');
    Route::group(['middleware' => ['auth']], function () {
        get('me', 'UserController@me');
        get('stats', 'UserController@stats');

        resource('users', 'UserController');

        post('contacts/{contacts}/send', 'ActionsController@send');
        post('directories/{directories}/send', 'ActionsController@sendToDirectory');

        Route::group(['prefix' => 'directories', 'namespace' => 'Directory'], function () {
            resource('{directories}/contacts', 'ContactController');
            resource('{directories}/imports', 'ImportController', ['only' => ['index', 'store', 'destroy']]);
            post('{directories}/imports/{imports}/process', 'ImportController@import');
        });

        Route::group(['namespace' => 'Contact'], function () {
            get('contacts/{contacts}/messages', 'MessageController@index');
        });

        Route::group(['namespace' => 'User'], function () {
            post('campaigns/{campaigns}/duplicate', 'CampaignController@duplicate');
            resource('purchases', 'PurchaseController');
            resource('campaigns', 'CampaignController');
            resource('directories', 'DirectoryController');
            resource('contacts', 'ContactController');
            get('messages', 'Message\MessageController@index');
        });

        Route::group(['namespace' => 'Campaign', 'prefix' => 'campaigns'], function () {
            resource('{campaigns}/directories', 'DirectoryController');
            resource('{campaigns}/messages', 'MessageController');
            resource('{campaigns}/contacts', 'ContactController');
            post('{campaigns}/send', 'ActionsController@sendCampaign');
        });
    });
});

get('/', function () {
    return redirect('/app/index.html');
});

get('/app/', function () {
    return redirect('/app/index.html');
});
