<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    /**
     * @param array $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
    	//TODO: throw exception instead
        $response = [
            'message' => 'validation error',
            'errors' => $errors
        ];
        return response()->json($response, 422);
    }
}
