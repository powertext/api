<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class ContactRepository extends Repository
{
    public function model()
    {
        return 'App\Contact';
    }
}
