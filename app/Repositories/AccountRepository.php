<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class AccountRepository extends Repository
{
    public function model()
    {
        return 'App\Account';
    }
}
