<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class DirectoryRepository extends Repository
{
    public function model()
    {
        return 'App\Directory';
    }
}
