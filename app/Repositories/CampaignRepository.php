<?php
namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class CampaignRepository extends Repository
{
    public function model()
    {
        return 'App\Campaign';
    }
}
