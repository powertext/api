<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Directory extends Model
{
    /**
     * Fillable fields
     * 
     * @var array
     */
    protected $fillable = [
        'user_id',
        'directory',
    ];

    /**
     * Appended data
     * 
     * @var array
     */
    protected $appends = [
        'contact_count'
    ];

    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * @return integer
     */
    public function getContactCountAttribute()
    {
        return $this->contacts()->count();
    }

    /**
     * @return Relation
     */
    public function campaigns()
    {
        return $this->belongsToMany('App\Campaign');
    }

    /**
     * @return Relation
     */
    public function contacts()
    {
        return $this->belongsToMany('App\Contact');
    }

    /**
     * @return Relation
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return Relation
     */
    public function imports()
    {
        return $this->hasMany('App\Import');
    }
}
