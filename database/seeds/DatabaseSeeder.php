<?php

use App\Campaign;
use App\Contact;
use App\Directory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call('UserTableSeeder');
        $this->directories();
        $this->campaigns();
        $this->contacts();

        // Model::reguard();
    }

    protected function directories()
    {
        Directory::truncate();

        for ($i = 1; $i <= 5; $i++) {
            Directory::create([
                'id' => $i,
                'account_id' => 1,
                'directory_name' => 'Random Directory',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ]);

            $this->command->info("Directory created $i/5");
        }
    }

    protected function campaigns()
    {
        Campaign::truncate();

        for ($i = 1; $i <= 5; $i++) {
            Campaign::create([
                'id' => $i,
                'account_id' => 1,
                'header' => 'Untitled',
                'message' => 'Some text coming from?',
                'is_sent' => ($i % 2 === 0),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ]);

            $this->command->info("Campaign created $i/5");
        }
    }

    protected function contacts()
    {
        Contact::truncate();
        DB::table('contact_directory')->truncate();

        for ($i = 1; $i <= 25; $i++) {
            Contact::create([
                'id' => $i,
                'account_id' => 1,
                'first_name' => 'John',
                // 'middle_name'   => 'Santos',
                'last_name' => 'Doe',
                'contact_number' => '639164417386',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
            ]);

            $directory = Directory::find(($i % 5) + 1);
            $directory->contacts()->attach($i);

            $this->command->info("Contacts created $i/25");
        }
    }
}
