<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\Profile;

class AddProperTopliteAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Profile::where('header', 'KEPPELAND')
            ->update([
                'header' => date('U')
            ]);

        $user = User::create([
            'semaphore_key' => 'QYMZosq5zyRpAMktf8UN',
            'email' => 'powertext.cares.000001@gmail.com',
            'password' => Hash::make('p0w3rt3xtc4r3s'),
            'name' => 'Keppeland'
        ]);

        Profile::create([
            'header' => 'Keppeland',
            'first_name' => 'Toplite',
            'last_name' => 'Inc',
            'phone' => date('U') . '-2',
            'user_id' => $user->id
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
