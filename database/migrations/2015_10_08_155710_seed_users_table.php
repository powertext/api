<?php

use App\Profile;
use App\User;
use Illuminate\Database\Migrations\Migration;

class SeedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = User::create([
            'email' => 'giansantillan18@gmail.com',
            'password' => Hash::make('Qweasd123:'),
            'name' => 'Gian Santillan',
        ]);
        $profile = Profile::create([
            'user_id' => $user->id,
            'header' => 'lightshire',
            'first_name' => 'Gian',
            'last_name' => 'Santillan',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
