<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Credit;
use App\User;
use App\Api\Services\Sms\SemaphoreSmsService;

class AddCredits extends Migration
{
    private $smsService;

    public function __construct()
    {
        $this->smsService = new SemaphoreSmsService;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $smsService = $this->smsService;
       $users = User::all();
       foreach ($users as $user) {
           $credits = $smsService->getCredits($user->semaphore_key);
           if ($credits->status === 'failure') {
               continue;
           }
           Credit::create([
                'user_id' => $user->id,
                'reference_id' => date('U'),
                'amount' => $credits->balance,
                'type' => 'earned'
           ]);
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
