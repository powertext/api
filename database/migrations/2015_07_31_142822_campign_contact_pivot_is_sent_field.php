<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampignContactPivotIsSentField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_contact', function(Blueprint $table)
        {
            $table->enum('status', ['delivered', 'pending', 'failed'], 'pending')->default('pending');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_contact', function(Blueprint $table)
        {
            $table->dropColumn('status');
        });
    }
}
