<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Profile;

class AddProfileForToplite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Profile::create([
            'user_id' => '2',
            'header' => 'KEPPELAND',
            'first_name' => 'Toplite',
            'last_name' => 'Development Inc',
            'phone' => date('U')
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
