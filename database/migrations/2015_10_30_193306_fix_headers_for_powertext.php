<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixHeadersForPowertext extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')
                ->where('id', 1)
                ->update([
                    'semaphore_key' => 'xYrni3tq4zswyJCkWsZx'
                ]);

        DB::table('users')
                ->where('id', 2)
                ->update([
                    'semaphore_key' => 'QYMZosq5zyRpAMktf8UN'
                ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
