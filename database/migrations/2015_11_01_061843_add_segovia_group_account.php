<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Profile;
use App\User;

class AddSegoviaGroupAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $user = User::create([
            'semaphore_key' => 'xYrni3tq4zswyJCkWsZx',
            'email' => 'powertextcares@gmail.com',
            'password' => Hash::make('Qweasd123:'),
            'name' => 'Powertext Cares'
        ]);        

        Profile::create([
            'header' => 'POWERTEXT',
            'user_id' => $user->id,
            'first_name' => 'Powertext',
            'last_name' => 'Cares',
            'phone' => date('U') . '1'          
        ]);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
