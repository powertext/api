<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imports', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('directory_id')->unsigned();
            $table->string('filename');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('number')->nullable();
            $table->string('columns')->nullable();
            $table->boolean('is_imported')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imports');
    }
}
