var BOWER_PATH = './bower_components';
var BUILD_PATH = './dist';
var SCRIPT_PATH = './src';
var STYLE_PATH = './styles';
var SASS_GLOB = '/**/*.scss';

var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');
var exec = require('child_process').exec;

gulp.task('scripts', function(cb) {
 /** webpack(require('./webpack.config'), function(err, stats) {
    if(err) throw new gutil.PluginError("webpack", err);
    gutil.log("[webpack]", stats.toString());
    cb();
  });*/
});
gulp.task('styles', function(cb) {
  exec('sass -r sass-globbing styles/main.scss:dist/style.css', function(err) {
    if ( err ) return cb(err);
    cb();
  });
});
gulp.task('fonts', function(cb) {
  return gulp.src(BOWER_PATH + '/font-awesome/fonts/*')
    .pipe(gulp.dest(BUILD_PATH + '/fonts'));
});

gulp.task('default', ['scripts', 'fonts', 'styles'], function() {
  // Not assigning the glob to a variable unlike SASS because
  // we don't have to.
//  gulp.watch(SCRIPT_PATH + '/**/*.{js,jsx,json,es6}', ['scripts']);
  gulp.watch([
    STYLE_PATH + SASS_GLOB,
    SCRIPT_PATH + SASS_GLOB
  ], ['styles']);
});

/*
gulp.task('build', ['build-scripts', 'build-styles', 'img']);
*/
