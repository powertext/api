## Web
> Repository for the Powertext Front End

### Requirements:
- [Node `>=0.12`](https://nodejs.org) / [npm](https://npm.com)
  - [gulp.js](http://gulpjs.com/)
- [Ruby](https://www.ruby-lang.org/en/) / [rubygems](https://rubygems.org/pages/download)
  - [Bundler](bundler.io)
  - [Neat](neat.bourbon.io)
  - [Bourbon](bourbon.io)

### Installation
```
npm install
bundler install

# install sass dependencies
cd styles && bourbon install && neat install
```

### Automation
```
gulp # Watches and build files for changes
```
