/**
 * Port of PHP's range() function
 * @param {number} min
 * @param {number} max
 * @return array
 */
export default (min, max) => {
  let stack = [];

  for ( let i = min; i < max; i++ ) {
    stack.push(i);
  }

  return stack;
};
