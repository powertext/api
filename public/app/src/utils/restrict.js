/**
 * Restrict n to exceed the min / max
 *
 * @params {int} n
 * @params {int} min
 * @params {int} max
 */
export default function(n, min, max) {
  if ( n < min ) {
    return min;
  } else if ( n > max ) {
    return max;
  }

  return n;
}
