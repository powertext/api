/**
 * Exception-thrower util
 * @param mixed val Value to be checked if undefined
 * @param string label Label of the value (parameter name)
 * @return undefined;
 */
export default function throwIfNull(val, label) {
  // Won't proceed, so we will omit the "return" keyword
  // to exit the function
  if ( label == null ) {
    throwIfNull(null, 'label');
  }

  if ( Array.isArray(val)) {
    val.map((v) => {
      throwIfNull(v);
    });
  } else {
    if ( val == null ) {
      throw new Error(`${label} must be defined, ${val} provided.`);
    }
  }
}
