import range from '../range';

export const CHARACTER_COUNT_PER_SMS = 160;

/**
 * @param {string}
 */
export const getSmsCount = text => text.length / CHARACTER_COUNT_PER_SMS;

/**
 *
 *
 * @param {string} text
 * @return {number}
 */
export const price = (text) => {
  try {
    return range(0, getSmsCount(text))
      .reduce(prev => prev + 1) + 1;
  } catch(e) {
    return 1;
  }
};


//fixThisShit
export const fixNumber = (state) => {  
   var _number = state.number.toString().replace(/\D/g, ''),
       data = {};
   switch (_number.length) {
       case 12:
            if (_number.indexOf('63') !== 0) {
                data.error = true;
            } else {
                data.error = false;
            }
            break;
       case 11:
            if (_number.indexOf('09') !== 0) {
                data.error = true;
            }
            else {
                var _split = _number.split('');
                _split[0] = '63';
                _number = _split.join('');
                data.error = false;
            }
            break;
       case 10:
            if (_number.indexOf('9') !== 0) {
                data.error = true;
            }
            else {
                var _split = _number.split('');
                _split[0] = '639';
                _number = _split.join('');
                data.error = false;
            }
            break;
       default:
         data.error = true;   
        console.log(_number.length); 
        break;   
   }
   data.number = _number;
   console.log(data);
   return data;
};
