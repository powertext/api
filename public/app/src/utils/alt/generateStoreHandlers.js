import ucFirst from '../ucFirst';

/**
 * Decorator for generateStoreHandler
 *
 * ES5:
 * generateStoreHandlers(['store', 'update'], { all: true })({});
 *
 * ES6:
 * @generateStoreHandlers(['store', 'update'], { all: true });
 * class UserActions {}
 *
 * @param {array}
 * @param {options}
 * @return function
 */
function decorator() {
  return (target) => {
    generateStoreHandlers.apply(null, [target].concat(arguments));
  }
}

/**
 * Helper to generate promise success-error handlers for Alt stores
 *
 * @param {object} Store
 * @param {array} actions
 * @param {object} options
 * @return none;
 */
function generateStoreHandlers(store, actions = [], options = {}) {
  actions.map((_action) => {
    const action = presentTense(action);
    store.prototype[`on${ucFirst(actions)}`] = function(data) {
      this.setState({
        [action]: false,
        [`${action}Error`]: false
      });
    };

    store.prototype[`on${ucFirst(actions)}Success`] = function(data) {
      this.setState({
        [action]: false,
        [`${action}Error`]: false
      });
    };
  });
}

/**
 * A lexic to work with:
 * fetchAll -> isFetchingAll
 * update -> isUpdating
 *
 * Assumes that string is camelCased
 *
 * @param {str}
 * @return {str}
 */
function presentTense(str) {
  const PRESENT_FORM = 'ing';
  const LINKING_VERB = 'is';
  const ALL_SUFFIX = 'All';
  let ALL_BOOL = false;
  let buffer = ucFirst(str);

  // Remove the occurence of All to be able
  // to get the string as it is.
  if ( str.indexOf('All') !== -1 ) {
    ALL_BOOL = true;
    buffer = buffer.substr(0, str.length - 3);
  }

  // Remove `e` to the verb to be able to concatenate -`ing`
  // update -> updat -> updating
  buffer = buffer.lastIndexOf('e') === buffer.length - 1
    ? buffer.substr(0, buffer.length - 1)
    : buffer;

  return [
    LINKING_VERB,
    buffer,
    PRESENT_FORM,
    ALL_BOOL ? ALL_SUFFIX : null
  ].join('');
}
