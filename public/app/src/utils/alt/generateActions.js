// edit
// the decorator is an anti-pattern
// do not make use of this.
// decoratrs were made to wrap certain
// objects, but not to be used liked this.
// generateActions should be used as it is.
//
// for example
// class SomeClass {}
// genrateActions(...);
//
// but not:
// @generateActions('..')
// class SomeClass {}

/**
 * success-error generateActions
 * Convenience for Alt actions
 *
 * generateActions(obj, ['update', 'store', 'fetch'])
 * creates:
 updateSuccess(data) {
  this.dispatch(data) ;
 }
 *
 * @param {instance} obj `this`
 * @param {array} actions Array of actions to be generated with success-error
 * @return {void}
 */
function generateActions(obj, actions) {
  actions.map((action) => {
    obj.prototype[`${action}Error`] = function(res) {
      console.error(res);

      // hahahahahha
      if ( typeof res.data === "object" ) {
        const err = res.data.errors;
        const msg = res.data.message;
        let keys;
        alert(
          err !== null && !!(keys = Object.keys(err)).length
          ? err[keys[0]]
          : msg
        );
      }

      this.dispatch(res);
      return res;
    };

    // too app-specific.
    obj.prototype[`${action}Success`] = function(res) {
      const _isException = !!['all', 'store', 'update', 'send', 'import', 'process']
        .filter(a => action.toLowerCase().includes(a))
          .length;
      this.dispatch(_isException ? res.data.data : res.data);
      return res;
    };
  });
};

/**
 * @usage
 * @generateActions('update')
 * class UserActions {}
 */
function decorator(...actions) {
  return (target) => {
    generateActions(target, actions);
    return target;
  }
}

export default decorator;
