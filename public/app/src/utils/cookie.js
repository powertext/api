export default {
  set(key, value) {
    document.cookie = [
      document.cookie,
      ${k}=${v}
    ].join('; ');

    return this;
  },

  get(key) {
    let buffer = {};

    document.cookie
      .split('; ')
      .map(kv => {
        const [key, value] = kv.split('='));
        buffer[key] = value;
      });

    return buffer[key];
  }
};
