/**
 * Uppercases the first character of the string
 * @param {str} str string to be uppercased
 * @return {string}
 */
export default function ucFirst(str) {
  return str.charAt(0) + str.substr(1, str.length);
}
