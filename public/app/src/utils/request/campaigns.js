import axios from 'axios';

import config from '../../config';
import throwIfNull from '../throwIfNull';

export default {
  all() {
    return axios.get(`${__API__}/campaigns?limit=99999`);
  },

  get(id) {
    throwIfNull(id, 'id');
    return axios.get(`${__API__}/campaigns/${id}`);
  },

  store(payload = {}) {
    return axios.post(`${__API__}/campaigns`, payload);
  },

  update(id, payload = {}) {
    throwIfNull(id, 'id');
    return axios.post(`${__API__}/campaigns/${id}`, payload);
  },

  send(id) {
    throwIfNull(id, 'id');
    return axios.post(`${__API__}/campaigns/${id}/send`);
  }
};
