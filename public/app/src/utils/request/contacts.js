import axios from 'axios';

import throwIfNull from '../throwIfNull';

export default {
  all() {
    return axios.get(`${__API__}/contacts/`);
  },

  get(id) {
    throwIfNull(id, 'id');
    return axios.get(`${__API__}/contacts/${id}`);
  },

  importFile(id, data) {
    throwIfNull(id, 'id');
    return axios.post(`${__API__}/directories/${id}/imports`, data);
  },

  processColumns(id, importId, data) {
    throwIfNull(id, 'id');
    throwIfNull(importId, 'importId');
    return axios.post(`${__API__}/directories/${id}/imports/${importId}/process`, data);
  },

  getSMSHistory(id) {
    throwIfNull(id, 'id');
    return axios.get(`${__API__}/contacts/${id}/messages`);
  }
};
