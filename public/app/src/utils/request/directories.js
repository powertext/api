import axios from 'axios';

import config from '../../config';
import throwIfNull from '../throwIfNull';

export default {
  all() {
    return axios.get(`${__API__}/directories?limit=99999`);
  },

  get(id) {
    throwIfNull(id, 'id');
    return axios.get(`${__API__}/directories/${id}`);
  },

  store(payload = {}) {
    return axios.post(`${__API__}/directories`, payload);
  },

  update(id, payload = {}) {
    throwIfNull(id, 'id');
    return axios.put(`${__API__}/directories/${id}`, payload);
  },

  del(id) {
    throwIfNull(id, 'id');
    return axios.del(`${__API__}/directories/${id}`);
  },


  /**
   * @param {number} Directory ID
   * @param {object}
   */
  import(id, data) {
    throwIfNull(id, 'directory id (cid)');
    return axios.post(`${__API__}/directories/${id}/import`, data);
  },

  /**
   * @param int dId Directory ID
   * @return Promise
   */
  allContacts(dId) {
    throwIfNull(dId, 'directory id (dId)');
    return axios.get(`${__API__}/directories/${dId}/contacts?limit=999999`);
  },

  /**
   * @param {int} dId Directory ID
   * @param {object} data
   * @return Promise
   */
  storeContact(dId, data) {
    throwIfNull(dId, 'directory id (dId)');
    return axios.post(`${__API__}/directories/${dId}/contacts`, data);
  },

  /**
   * @param {number} id Contact ID
   */
  getContact(id) {
    throwIfNull(id, 'contact id (id)');
    return axios.get(`${__API__}/contacts/${id}`);
  },

  /**
   * @param {number} id Contact ID
   * @param {object}
   */
  updateContact(id, data) {
    // throwIfNull(did, 'directory id (did)');
    throwIfNull(id, 'contact id (cid)');
    return axios.put(`${__API__}/contacts/${id}`, data);
  }
};
