import axios from 'axios';

import config from '../../config';
import throwIfNull from '../throwIfNull';
import qs from 'querystring';
import 'axios-response-logger';

export default {
    login(loginCreds = {}) {
        return axios.post(`${__API__}/auth`, loginCreds);
    }
}
