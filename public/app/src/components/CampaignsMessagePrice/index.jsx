import React, { PropTypes } from 'react';
import { price } from '../../utils/semaphore';

export default class CampaignsMessagePrice extends React.Component {
  static propTypes = {
    message: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { message } = this.props;

    return (
      <span className="c-label -info -small">
        <span className="emphasized"><i className="fa fa-mobile" /></span>
        {price(message)} <span className="lessemphasis">Credit</span>
      </span>
    );
  }
}
