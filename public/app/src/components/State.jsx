import React, { PropTypes } from 'react';

export default class State extends React.Component {
  static propTypes = {
    /**
     * Initial state
     */
    init: PropTypes.object,

    /**
     * An alternative to this.props.children
     * e.g.,
     (state, ctx) => {
        return (
          <span>
            {state}
            <input type="text" valueLink={linkState(ctx, 'name')} />
          </span>
        );
     }
     */
    render: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = this.props.init || {};
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps.init);
  }

  render() {
    return this.props.render(this.state, this);
  }
}
