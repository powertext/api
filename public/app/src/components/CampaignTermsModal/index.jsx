import React, {PropTypes} from 'react';
import classnames from 'classnames';

export default class CampaignTermsModal extends React.Component {
   
   constructor(props, context) {
        super(props, context);
        this.exportPublicMethods = {
            open: ::this.open
        };
   }

    render() {
        return (
         <div>
            <input onChange={::this.stateChange}  className='modal-state' ref='modal_state'  id='terms-modal' type='checkbox' />
            <div className='modal-fade-screen'>
                <div className='modal-inner'>
                    <div className='modal-close' onClick={::this.close} forHTML='terms-modal' />
                    <h1>Terms & Conditions</h1>
                    <div className='content'>
                        <p>By sending this message, the user (“you”) hereby indemnify the Powertext.co and its owners, shareholders, directors, officers, employees, consultants, suppliers, and affiliates (collectively called “we” or “us”) for any liability, suit or damage caused by sending this message and/or the content in the message.</p>
                        <p>By sending this message, the user (“you”) hereby indemnify the Powertext.co and its owners, shareholders, directors, officers, employees, consultants, suppliers, and affiliates (collectively called “we” or “us”) for any liability, suit or damage caused by sending this message and/or the content in the message.</p>
                        <ul>
                            <li>(1) Political or Election Campaigns, Political Propaganda or the advocacy of any political cause;</li>
                            <li>(2) Pornographic or illicit material or activity of any type;</li>
                            <li>(3) Escort Services</li>
                            <li>(4) Gambling activities, including but not limited to virtual casinos and sports betting; as well as other electronic games of chance whether or not the same are or would be considered gambling as defined by existing laws;</li>
                            <li>(5) The promotion, sales, and delivery of Firearms, Ammunitions and explosives, dangerous drugs, or restricted or prohibited medicines or medical aids;</li>
                            <li>(6) Pyramid Selling, Multi-Level Marketing and Commissions</li>
                            <li>(7) The sale of securities, as the same is defined under the Philippines’ Revised Securities Act</li>
                            <li>(8) Person-to-Person Messaging, Chat or acts of personal dialogue</li>
                            <li>(9) Spamming or any other obtrusive and disruptive activities;</li>
                            <li>(10)    Any activity, whether or not analogous to the foregoing, that is detrimental to Powertext.co and its owners, shareholders, directors, officers, employees, consultants, suppliers, and affiliates</li>
                        </ul>
                        <p>You agree that sending this message, that it’s content has been approved by government agencies and/or regulating bodies within your country and region that regulates text messaging and commercial advertising and promotions via text messaging.</p>
                        <p>You agree that sending this message, that it’s content has been approved by government agencies and/or regulating bodies within your country and region that regulates text messaging and commercial advertising and promotions via text messaging.</p>
                        <p>You agree that sending this message, that it’s content has been approved by government agencies and/or regulating bodies within your country and region that regulates text messaging and commercial advertising and promotions via text messaging.</p>
                        <ul>
                            <li>(1) the recipient’s mobile phone being outside the territory of the Philippines</li>
                            <li>(2) the recipient’s mobile phone has been shut off for an extended period of time, which cancels any incoming messages it was queued to receive</li>
                            <li>(3) the recipient’s mobile phone blocks off incoming messages of which included the message sent to his/her number</li>
                            <li>(4) the recipient’s mobile phone deleted the message automatically</li>
                        </ul>
                    </div>
                    <button className='c-btn -success'onClick={::this.close}>Close</button>
                </div>
            </div>
         </div>
        );
    }

    open() {
    //    this.refs.modal_state.checked = true;         
        this.refs.modal_state.getDOMNode().checked = true;
        this.stateChange();
    }

    close() {
        this.refs.modal_state.getDOMNode().checked = false;
        this.stateChange();
    }

    stateChange(evt) {
        if (this.refs.modal_state.getDOMNode().checked) {
            document.body.classList.add('modal-open');       
        } else {
            document.body.classList.remove('modal-open');
        }
    }
}
