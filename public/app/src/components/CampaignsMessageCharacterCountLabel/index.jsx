import React, { PropTypes } from 'react';
import classnames from 'classnames';
import { CHARACTER_COUNT_PER_SMS } from '../../utils/semaphore';

export default class CampaignsCharacterCountLabel extends React.Component {
  static propTypes = {
    message: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { message } = this.props;
    var total = message.length / CHARACTER_COUNT_PER_SMS;
    return (
      <span className={classnames('c-label -small', {
        '-info': message.length < 50,
        '-warning': message.length > 50 && message.length < CHARACTER_COUNT_PER_SMS,
        '-danger': message.length > CHARACTER_COUNT_PER_SMS
      })}>
        <span className="emphasized"><i className="fa fa-font" /></span>
        {message.length}&#47;{Math.ceil(total) * CHARACTER_COUNT_PER_SMS} <span className="lessemphasis">Characters</span> or {Math.ceil(total)} Credits per Message
      </span>
    );
  }
}
