import React, { PropTypes } from 'react';
import classnames from 'classnames';

/**
 * @usage
 * <CampaignStatus status={campaign.get('is_sent')} />
 */
export default class CampaignsStatus extends React.Component {
  static propTypes = {
    /**
     * If the campaign has already been sent
     * [published, draft]
     */
    status: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { status } = this.props;

    return (
      <div className={classnames('c-label -small', {
        '-warning': !status,
        '-success': status
      })}>
        {status ? 'Published' : 'Draft'}
      </div>
    );
  }
}
