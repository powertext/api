import React from 'react';

/**
 * @usage
 * selectionItem(MyComponent);
 *
 * or
 *
 * @selectionItem
 * export default class MyComponent ..
 */
export default function (Component) {
  // Get the component static `wizard`
  const options = Component.wizard || {};

  return class SelectionItem extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        expanded: options.expanded || false
      }
    }

    render() {
      const { expanded } = this.state;

      return (
        <Component
          {/* functions */}
          toggleExpand={::this._toggle}
          expand={this._toggle.bind(null, true)}
          close={this._toggle.bind(null, false)}
          {/* state */}
          expanded={expanded}
          {...this.props} />
      );
    }

    /**
     * Jump to the given step
     */
    _toggleExpand(status) {
      this.setState({ expanded: status || !this.state.expanded });
    }
  }
}
