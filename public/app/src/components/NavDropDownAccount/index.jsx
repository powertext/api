import React from 'react';
import SessionStore from '../../stores/SessionStore';
import connectToStores from 'alt/utils/connectToStores'

//@connectToStores
export default class NavDropDownAccount extends React.Component {
    static contextTypes = {
        router: React.PropTypes.object.isRequired
    };

    static propTypes = {
        auth: React.PropTypes.object
    };

    constructor(props, context) {
        super(props, context);
        
        console.log('props: ', props);    
    }

    render() {
       return (
           <div className='dropdown'>
                <div className='dropdown-container'>
                    <p className='dropdown-button' onClick={::this._handleDropDownClick}>{this.props.auth.data.name}</p>
                    <ul className='dropdown-menu  dropdown-select'>
                       <li>Number of Credits <span className='badge-alert'>{this.props.auth.data.available_credits}</span></li>
                        <li>Account Profile</li>
                        <li>Change Password</li>
                        <li onClick={::this._logout}>Logout</li>
                    </ul>
                </div>
           </div>
       );
    }

    _handleDropDownClick(evt) {
        var btn = evt.currentTarget;
        var menu = btn.nextSibling;
        //hello world
        if (menu.classList.contains('show-menu')) {
            menu.className=menu.className.split('show-menu').join(" ");
        } else {
            menu.classList.add('show-menu');
        }

    }

    _logout(evt) {
        SessionStore.logout();
        this.context.router.transitionTo('login');
    }
}
