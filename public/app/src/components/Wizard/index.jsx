import React from 'react';

// first step in the wizard
const STARTING_STEP = 1;

/**
 * @usage
 * wizard(MyComponent);
 *
 * or
 *
 * @wizard
 * export default class MyComponent ..
 */
export default function (Component) {
  // Get the component static `wizard`
  const options = Component.wizard || {};

  if ( options.steps == null ) {
    throw new Error('\`options.steps\` must be defined, <${options.steps}> provided.');
  }

  return class Wizard extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        // Current active step
        step: STARTING_STEP,
        // Last highest recorded step.
        // So we can validate if the user
        // is skipping a step
        last: STARTING_STEP
      }
    }

    render() {
      const { step, last } = this.state;

      return (
        <Component
          {/* functions */}
          jump={::this.jump}
          next={::this._next}
          previous={this.._previous}
          {/* state */}
          step={step}
          last={last}
          {/* state convenience */}
          isLast={step === options.steps}

          {...this.props} />
      );
    }

    /**
     * Jump to the given step
     */
    _jump(step) {
      if ( options.steps && step > options.steps ) {
        throw new Error(`Provided step <${step}> must not exceed the set number of steps.`);
      } else if ( step < 0) {
        throw new Error(`Provided step <${step}> is less than the starting point <0>.`);
      }

      const { last } = this.state;

      this.setState({
        step: step,
        last: step > last ? step : last
      });
    }

    /**
     * Convenience method to jump to the next step
     */
    _next() {
      this._jump(this.state.step + 1);
    }

    /**
     * Convenience method to jump to the previous step
     */
    _previous() {
      this._jump(this.state.step - 1);
    }
  }
}
