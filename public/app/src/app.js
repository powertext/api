require('babel/polyfill');
import jwt from './utils/axios/jwt';
import React from 'react';
import { Resolver } from 'react-resolver';
import routes from './routes';
import render from 'react-dom';
import { Router  } from 'react-router';


React.render(routes, document.getElementById('root'));
//Resolver.render(routes, document.getElementById('root'));
