import { createActions } from 'alt/utils/decorators';

import alt from '../alt';
import generateActions from '../utils/alt/generateActions';
import axios from 'axios';
import config from '../config';
import SessionAPIUtil from '../utils/request/login';

@createActions(alt)
class SessionActions {

  get() {
    if (localStorage.getItem(config.AUTH_KEY) == null) {
      return null; 
    }

    return axios.get(`${__API__}/me`)
      .then(::this.actions.getSuccess)
      .catch(::this.actions.getError);
  }

  getSuccess(res) {
	  this.dispatch(res.data);
  }

  getError(res) {
	console.log(res);
 	  this.dispatch(res.data);
  }

  login(state) {
      return SessionAPIUtil.login(state)
          .then(::this.actions.loginSuccess)
          .catch(::this.actions.loginError)
  }

  loginSuccess(res) {
      this.dispatch(res.data);
  }

  loginError(res) {
      this.dispatch(res.data);
  }
}

export default SessionActions;
