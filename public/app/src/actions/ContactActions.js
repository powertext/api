import { createActions } from 'alt/utils/decorators';

import alt from '../alt';
import generateActions from '../utils/alt/generateActions';
import DirectoriesAPIUtil from '../utils/request/directories';

@createActions(alt)
@generateActions('fetchAll', 'fetch', 'store', 'update', 'remove')
class ContactActions {
  constructor() {
    this.generateActions(
      'select',
      'selectAll',
      'deleteSelected',
      'selectDirectory',
      'selectContact',
      'sort'
    );
  }

  /**
   * @param {int} id direcotryId
   */
  fetchAll(id) {
    this.dispatch();

    return DirectoriesAPIUtil.allContacts(id)
      .then(::this.actions.fetchAllSuccess)
      .catch(::this.actions.fetchAllError);
  }

  /**
   * @param {int} id Contact ID
   */
  fetch(id) {
    this.dispatch();

    return DirectoriesAPIUtil.getContact(id)
      .then(::this.actions.fetchSuccess)
      .catch(::this.actions.fetchError);
  }

  /**
   * @param {int} id Directory ID
   * @param {object} data
   */
  store(id, data) {
    return DirectoriesAPIUtil.storeContact(id, data)
      .then(::this.actions.storeSuccess)
      .catch(::this.actions.storeError)
  }

  /**
   * @param {number} directoryId Directory ID
   * @param {number} id Contact ID
   * @param {object}
   */
  update(directoryId, id, data) {
    // todo, make DirectoryStore listen to tis.
    return DirectoriesAPIUtil.updateContact(id, data)
      .then(::this.actions.storeSuccess)
      .catch(::this.actions.storeError)
  }
}

export default ContactActions;
