import { createActions } from 'alt/utils/decorators';

import alt from '../alt';
import generateActions from '../utils/alt/generateActions';
import ContactAPIUtil from '../utils/request/contacts';

@createActions(alt)
@generateActions('fetchAll')
class ContactHistoryActions {
  fetchAll(id) {
    return ContactAPIUtil.getSMSHistory(id)
      .then(::this.actions.fetchAllSuccess)
      .catch(::this.actions.fetchAllError);
  }
}

export default ContactHistoryActions;
