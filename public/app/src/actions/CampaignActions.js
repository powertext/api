import { createActions } from 'alt/utils/decorators';

import alt from '../alt';
import generateActions from '../utils/alt/generateActions';
import CampaignAPIUtil from '../utils/request/campaigns';

@createActions(alt)
@generateActions('fetchAll', 'fetch', 'store', 'send')
class CampaignActions {
  constructor() {
    this.generateActions(
      'select',
      'selectAll',
      'deleteSelected',
      'sort'
    );
  }

  fetchAll() {
    return CampaignAPIUtil.all()
      .then(::this.actions.fetchAllSuccess)
      .catch(::this.actions.fetchAllError);
  }

  fetch(id) {
    return CampaignAPIUtil.get(id)
      .then(::this.actions.fetchSuccess)
      .catch(::this.actions.fetchError);
  }

  store(data) {
    return CampaignAPIUtil.store(data)
      .then(::this.actions.storeSuccess)
      .catch(::this.actions.storeError);
  }

  send(id) {
    return CampaignAPIUtil.send(id)
      .then(::this.actions.sendSuccess)
      .catch(::this.actions.sendError);
  }
}

export default CampaignActions;
