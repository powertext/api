import { createActions } from 'alt/utils/decorators';

import alt from '../alt';
import generateActions from '../utils/alt/generateActions';
import DirectoryAPIUtil from '../utils/request/directories';


@createActions(alt)
@generateActions('fetchAll', 'fetch', 'store', 'update', 'delete')
class DirectoryActions {
  constructor() {
    this.generateActions(
      'create',
      'delete',
      'select',
      'selectAll',
      'selectContact',
      'selectDirectory',
      'deleteSelected',
      'sort'
    );
  }

  fetchAll() {
    this.dispatch();

    return DirectoryAPIUtil.all()
      .then(::this.actions.fetchAllSuccess)
      .catch(::this.actions.fetchAllError);
  }

  fetch(id) {
    this.dispatch();

    return DirectoryAPIUtil.get(id)
      .then(::this.actions.fetchSuccess)
      .catch(::this.actions.fetchError);
  }

  store(data) {
    this.dispatch();

    return DirectoryAPIUtil.store(data)
      .then(::this.actions.storeSuccess)
      .catch(::this.actions.storeError);
  }

  update(id, data) {
    this.dispatch();

    return DirectoryAPIUtil.update(id, data)
      .then(::this.actions.updateSuccess)
      .catch(::this.actions.updateError);
  }
}

export default DirectoryActions;
