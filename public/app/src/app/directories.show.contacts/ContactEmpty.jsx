import React, { PropTypes } from 'react';
import { Link } from 'react-router';

export default class ContactEmpty extends React.Component {
  static propTypes = {
    directoryId: PropTypes.number.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <section>
        <h1>There are no contacts yet!</h1>
        <Link to={`/directories/${this.props.directoryId}/contacts/add`}>
          Why don't you create one?
        </Link>
      </section>
    );
  }
}
