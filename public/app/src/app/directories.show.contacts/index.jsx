import AltContainer from 'alt/AltContainer';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import ContactStore from '../../stores/ContactStore';
import ContactActions from '../../actions/ContactActions';
import ContactMenu from './ContactMenu';
import ContactTable from './ContactTable';
import ContactEmpty from './ContactEmpty';

export default class DirectoriesShowContacts extends React.Component {
  static propTypes = {
    /**
     * List of contacts
     * @store ContactStore
     */
    contacts: PropTypes.object.isRequired,

    /**
     * Directory ID
     */
    directoryId: PropTypes.number.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { contacts, directoryId } = this.props;
    const sort = (a, b) => a.get('name') > b.get('name');
    let filteredContacts = !ContactStore.isSortAscending() && !ContactStore.isSortDescending()
      ? contacts
      : (ContactStore.isSortAscending() ? contacts.sort(sort) : contacts.sort(sort).reverse());

    return (
      <section>
        <div className="c-top-menu -no-allowance-top">
          <div className="left">
            <h1>Contacts</h1>
          </div>

          <div className="right">
            <Link to={`/directories/${directoryId}/contacts/import`} className="c-btn -info">Import Contacts</Link>
            <Link to={`/directories/${directoryId}/contacts/add`} className="c-btn -info">Add Contact</Link>
          </div>
        </div>

        {
          contacts.size >= 1
          ? (
            <section>
              <ContactMenu isAnySelected={ContactStore.isAnySelected()} />
              <ContactTable
              contacts={filteredContacts}
              directoryId={directoryId} />
            </section>
          ) : <ContactEmpty directoryId={directoryId} />
        }
      </section>
    );
  }
}
