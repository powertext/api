import React, { PropTypes } from 'react';

import ContactMenuSortButton from './ContactMenuSortButton';
import ContactMenuDeleteButton from './ContactMenuDeleteButton';

export default class ContactMenu extends React.Component {
  static propTypes = {
    isAnySelected: PropTypes.bool.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <section className="c-top-menu">
        <section className="g-col-3">
          <ContactMenuSortButton />
        </section>

        <section className="g-col-3">
          <ContactMenuDeleteButton isAnySelected={this.props.isAnySelected} />
        </section>
      </section>
    );
  }
}
