import React, { PropTypes } from 'react';

import ContactActions from '../../actions/ContactActions';
import ContactStore from '../../stores/ContactStore';

export default class ContactTableHeadSelect extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <th>
        <input type="checkbox"
          checked={ContactStore.isAllSelected()}
          onChange={::this._onChangeHandle} />
      </th>
    );
  }

  _onChangeHandle() {
    ContactActions.selectAll();
  }
}
