import React, { PropTypes } from 'react';
import { Link } from 'react-router';

export default class ContactTableRowHistoryLink extends React.Component {
  static propTypes = {
    contactId: PropTypes.number.isRequired,
    directoryId: PropTypes.number.isRequired
  };

  render() {
    const { contactId, directoryId } = this.props;

    return (
      <Link
        to={`/directories/${directoryId}/contacts/${contactId}/history`}
        className="c-btn -info">
        History
      </Link>
    );
  }
}
