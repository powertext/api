import React from 'react';

import ContactActions from '../../actions/ContactActions';
import ContactStore from '../../stores/ContactStore';

export default class ContactMenuSortButton extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return ContactStore.getState().contacts.size >= 1 ? (
      <button onClick={::this._onClickHandle} className="c-btn -info">
        {this.renderText()}
      </button>
    ) : null;
  }

  renderText() {
    if ( ContactStore.isSortAscending() ) {
      return (
        <span className="pre-icon">
          <i className="fa fa-caret-down" />
          Ascending
        </span>
      );
    } else if ( ContactStore.isSortDescending() ) {
      return (
        <span className="pre-icon">
          <i className="fa fa-caret-down" />
          Descending
        </span>
      );
    }

    return 'Sort';
  }

  _onClickHandle() {
    ContactActions.sort();
  }
}
