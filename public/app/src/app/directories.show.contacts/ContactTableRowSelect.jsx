import React, { PropTypes } from 'react';

import ContactActions from '../../actions/ContactActions';
import ContactStore from '../../stores/ContactStore';

export default class ContactTableRowSelect extends React.Component {
  static propTypes = {
    id: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <td>
        <input type="checkbox"
          checked={ContactStore.isSelected(this.props.id)}
          onChange={::this._onChangeHandle} />
      </td>
    );
  }

  _onChangeHandle() {
    ContactActions.select(this.props.id);
  }
}
