import React, { PropTypes } from 'react';

import ContactTableHeadSelect from './ContactTableHeadSelect';
import ContactTableRow from './ContactTableRow';

export default class ContactTable extends React.Component {
  static propTypes = {
    contacts: PropTypes.object.isRequired,
    directoryId: PropTypes.number.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { contacts, directoryId } = this.props;

    return (
      <table className="table -hovered">
        <thead>
          <tr>
            <ContactTableHeadSelect />
            <th>Contact Number</th>
            <th>First Name</th>
            {/* <th>Middle Name</th> */}
            <th>Last Name</th>
            <th>No. of SMS Sent</th>
            <th>Message History</th>
          </tr>
        </thead>

        <tbody>
          {contacts.map((contact, i) => {
            return <ContactTableRow
              key={i}
              contact={contact}
              directoryId={directoryId} />;
          })}
        </tbody>
      </table>
    );
  }
}
