import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import ContactTableRowSelect from './ContactTableRowSelect';
import ContactTableRowHistoryLink from './ContactTableRowHistoryLink';

export default class ContactTableRow extends React.Component {
  static propTypes = {
    contact: PropTypes.object.isRequired,
    directoryId: PropTypes.number.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { contact, directoryId } = this.props;

    return (
      <tr>
        <ContactTableRowSelect id={contact.get('id')} />
        <td>
          <Link to={`/directories/${directoryId}/contacts/${contact.get('id')}`}>
            {contact.get('number')}
          </Link>
        </td>
        <td>{contact.get('first_name')}</td>
        {/* <td>{contact.get('middle_name')}</td> */}
        <td>{contact.get('last_name')}</td>
        <td>{contact.get('message_count')}</td>
        <td>
          <ContactTableRowHistoryLink
            contactId={contact.get('id')}
            directoryId={directoryId} />
        </td>
      </tr>
    );
  }
}
