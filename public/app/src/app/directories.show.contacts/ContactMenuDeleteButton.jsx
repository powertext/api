import React, { PropTypes } from 'react';

import ContactActions from '../../actions/ContactActions';
import ContactStore from '../../stores/ContactStore';

export default class ContactMenuDeleteButton extends React.Component {
  static propTypes = {
    isAnySelected: PropTypes.bool.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return this.props.isAnySelected ? (
      <button type="button" className="c-btn -danger" onClick={::this._onClickHandle}>
        <i className="fa fa-trash pre-icon" />
        Delete
      </button>
    ) : null;
  }

  _onClickHandle() {
    ContactActions.deleteSelected();
  }
}
