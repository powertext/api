import React from 'react';
import { Link } from 'react-router';

export default class HistoryEmpty extends React.Component {
  render() {
    return (
      <section>
        <h1>Oops!</h1>
        <h4>This user doesn't have any history yet.</h4>
      </section>
    );
  }
}
