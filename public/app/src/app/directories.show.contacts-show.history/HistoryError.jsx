import React from 'react';
import { Link } from 'react-router';

export default class HistoryError extends React.Component {
  render() {
    return (
      <section>
        <h1>:(</h1>
        <h4>An error occured while loading the history.</h4>
      </section>
    );
  }
}
