import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import moment from 'moment';

import CampaignsStatus from '../../components/CampaignsStatus';

export default class HistoryTable extends React.Component {
  static propTypes = {
    messages: PropTypes.arrayOf(
      PropTypes.object
    ).isRequired
  };

  render() {
    return (
      <table className="table -hovered">
        <thead>
          <tr>
            <td>Date Created</td>
            <th>Message</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          {this.props.messages.map((message, i) => (
            <tr key={i}>
              <td>{moment(message.get('created_at')).format('MMMM Do YYYY, h:mm:ss a')}</td>
              <td>{this.renderMessage(message.get('message'))}</td>
              <td><CampaignsStatus status={message.get('status')} /></td>
              <td><Link to={`/campaigns/${message.get('campaign_id')}`} className="c-btn -info">Visit Campaign</Link></td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  renderMessage(msg = '') {
    return msg.length > 60
      ? `${msg.substr(0, 60)}...`
      : msg;
  }
}
