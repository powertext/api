import React from 'react';
import AltContainer from 'alt/AltContainer';

import HistoryTable from './HistoryTable';
import HistoryEmpty from './HistoryEmpty';
import HistoryError from './HistoryError';
import HistoryLoading from './HistoryLoading';

import ContactHistoryActions from '../../actions/ContactHistoryActions';
import ContactHistoryStore from '../../stores/ContactHistoryStore';
import ContactStore from '../../stores/ContactStore';
import ContactActions from '../../actions/ContactActions';

export default class DirectoriesShowContactsShowHistory extends React.Component {
  constructor(props, context) {
    super(props, context);

    ContactActions.fetch(this.props.params.contactId);
    ContactHistoryActions.fetchAll(this.props.params.contactId);
  }

  render() {
    return <AltContainer
      stores={[ContactStore, ContactHistoryStore]}
      inject={{
        ContactStore: () => ContactStore.getState(),
        ContactHistoryStore: () => ContactHistoryStore.getState()
      }}
      render={(props) => {
        return (
          <section>
            <div className="c-top-menu -no-allowance-top">
              <div className="left">
                <h1>{props.ContactStore.contact.get('number')} History</h1>
              </div>
            </div>

            {this.renderTable(props.ContactHistoryStore)}
          </section>
        );
      }} />;
  }

  renderTable({ messages, isFetchingAll, isFetchingAllError }) {
    if ( isFetchingAll ) {
      return <HistoryLoading />;
    }

    if ( isFetchingAllError ) {
      return <HistoryError />;
    }

    if ( !messages.size ) {
      return <HistoryEmpty />;
    }

    return <HistoryTable messages={messages} />;
  }
}
