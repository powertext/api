import React, { PropTypes } from 'react';
import { Link } from 'react-router';

export default class EditButton extends React.Component {
  static propTypes = {
    /**
     * Campaign ID
     */
    id: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
        <Link to={`/campaigns/${this.props.id}/edit`} className="c-btn -info">
          <span className="pre-icon"><i className="fa fa-pencil" /></span>
          Edit
        </Link>
    );
  }
}
