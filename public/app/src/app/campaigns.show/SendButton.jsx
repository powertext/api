import React, { PropTypes } from 'react';
import CampaignActions from '../../actions/CampaignActions';

export default class SendButton extends React.Component {
  static propTypes = {
    /**
     * Campaign ID
     */
    id: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button type="button" className="c-btn -success" onClick={::this._onClickHandle}>
        <span className="pre-icon"><i className="fa fa-paper-plane-o" /></span>
        Send
      </button>
    );
  }

  _onClickHandle() {
    const confirmed = confirm('Are you sure to send this campaign?');

    if ( !confirmed ) {
      return;
    }

    CampaignActions.send(this.props.id)
      .then((res) => {
        alert('Message has been sent!');
        return res;
      });
  }
}
