import AltContainer from 'alt/AltContainer';
import React, { PropTypes } from 'react';
import { resolve } from 'react-resolver';
import { Link } from 'react-router';

import CampaignActions from '../../actions/CampaignActions';
import CampaignStore from '../../stores/CampaignStore';
import CampaignsStatus from '../../components/CampaignsStatus';
import CampaignsMessageCharacterCountLabel from '../../components/CampaignsMessageCharacterCountLabel';
import CampaignsMessagePrice from '../../components/CampaignsMessagePrice';
import SendButton from './SendButton';
import EditButton from './EditButton';

@resolve({
    campaign({ params }) {
      const { campaignId: id } = params;
      return CampaignActions.fetch(id);
    } 
})
export default class CampaignsShow extends React.Component {
  static displayName = 'CampaignsShow';

  static contextTypes = {
    /**
     * Router instance
     */
    router: PropTypes.object.isRequired
  };

  render() {
    return <AltContainer
      store={CampaignStore}
      render={({ campaign }) => {
        return (
          <section>
            <div className="breadcrumbs">
              <Link to={`/campaigns`}>Campaigns</Link>
              <span>{campaign.get('name')}</span>
            </div>

            <div className="c-top-menu -no-allowance-top">
              <div className="left">
                <h1>{campaign.get('name')}</h1>
                <CampaignsStatus status={campaign.get('is_sent')} />
              </div>

              <div className="right">
                <SendButton id={campaign.get('id')} />
                <EditButton id={campaign.get('id')} />
              </div>
            </div>

            <section className="g-wide">
              <div className="typo-hr" />
              <section className="form-container">
                <label>Message</label>
                <p>{campaign.get('message')}</p>
              </section>
              <section className="g-bottom-space-narrow">
                <CampaignsMessageCharacterCountLabel message={campaign.get('message')} />
                <CampaignsMessagePrice message={campaign.get('message')} />
              </section>
            </section>
          </section>
        );
      }} />;
  }
}

/**export default Resolver.createContainer(CampaignsShow, {
  resolve: {
    campaign({ params }) {
      const { campaignId: id } = params;
      return CampaignActions.fetch(id);
    }
  }
});**/
