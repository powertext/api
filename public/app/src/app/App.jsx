import React from 'react';
import { resolve } from "react-resolver";
import SessionActions from '../actions/SessionActions';
/**
 * Top-level component
 */

@resolve({
    auth() {
        return SessionActions.get();    
    },

    children(props) {
        console.log(props);
        return props.children;
    }
    
})
export default class App extends React.Component {
 static displayName = "App";

  
  constructor(props) {
    super(props);
  }

  componentDidMount() {
	  console.log(this.context);
  }

  render() {
      return this.props.children;
  }

}



