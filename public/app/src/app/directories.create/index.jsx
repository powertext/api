import React from 'react';
import { Link } from 'react-router';
import AltContainer from 'alt/AltContainer';

import linkState from '../../utils/linkState';
import State from '../../components/State';
import DirectoryAPIUtil from '../../utils/request/directories';
import DirectoryStore from '../../stores/DirectoryStore';
import DirectoryActions from '../../actions/DirectoryActions';

export default class DirectoriesCreate extends React.Component {
  static contextTypes = {
    /**
     * Router instance
     */
    router: React.PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="directories-create">
        <div className="breadcrumbs">
          <Link to={'/directories/'}>Directories</Link>
          <span>New Directory</span>
        </div>

        <div className="c-top-menu">
          <div className="left">
            <h2>Create Directory</h2>
          </div>
        </div>

        <State
          init={{ name: '' }}
          render={(state, ctx) => (
            <form onSubmit={::this._onSubmitHandle(state)}>
              <div className="form-container g-wide">
                <label htmlFor="directory-name">Directory Name</label>
                <input type="text"
                  name="name"
                  id="directory-name"
                  className="form-control"
//                  onBlur={::this.directoryNameBlur(state)}
                  valueLink={linkState(ctx, 'name')} />
              </div>

              <AltContainer
                store={DirectoryStore}
                render={({ isStoring }) => (
                  <button className="c-btn -info" disabled={isStoring}>
                    Create
                  </button>
                )} />
            </form>
          )} />
      </div>
    );
  }

  

  _onSubmitHandle(state) {
    return (evt) => {
      evt.preventDefault();
      evt.stopPropagation && evt.stopPropagation();

      DirectoryAPIUtil.store(state)
        .then((res) => {
          this.context.router
            .transitionTo(`/directories/${res.data.data.id}`);
        });
    }
  }
}
