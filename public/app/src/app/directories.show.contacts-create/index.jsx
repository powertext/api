import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import ContactStore from '../../stores/ContactStore';
import ContactActions from '../../actions/ContactActions';
import State from '../../components/State';
import linkState from '../../utils/linkState';
import ImportAlertBox from './ImportAlertBox';
import { fixNumber }  from '../../utils/semaphore';

class DirectoriesShowContactsCreate extends React.Component {
  static contextTypes = {
    /**
     * Router instance
     */
    router: PropTypes.object.isRequired
  };

  static propTypes = {
    // router params
    params: PropTypes.object.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { directoryId } = this.props.params;

    return (
      <div>
        <div className="c-top-menu -no-allowance-top">
          <div className="left">
            <h1>New Contact</h1>
          </div>
        </div>

        <div className="g-wide">
          <ImportAlertBox directoryId={directoryId} />

          <State
            init={{
              number: '',
              first_name: '',
              last_name: ''
            }}
            render={(state, ctx) => (
              <form onSubmit={::this._onSubmitHandle(state)}>
                <div className="form-container">
                  <label htmlFor="new-contact-number">Mobile Number</label>
                  <input type="text"
                    id="new-contact-number"
                    name="number"
                    className="form-control"
                    onBlur={::this.handleMobileNumber(state)}
                    valueLink={linkState(ctx, 'number')}
                    required />
                </div>
                <div className="form-container">
                  <label htmlFor="new-contact-fn">First Name</label>
                  <input type="text"
                    id="new-contact-fn"
                    name="first_name"
                    className="form-control"
                    valueLink={linkState(ctx, 'first_name')}
                    required />
                </div>

                <div className="form-container">
                  <label htmlFor="new-contact-ln">Last Name</label>
                  <input type="text"
                    id="new-contact-ln"
                    name="last_name"
                    className="form-control"
                    valueLink={linkState(ctx, 'last_name')}
                    required />
                </div>

                <div className="c-action-section">
                  <Link to={`/directories/${directoryId}`} className="left c-btn">Cancel</Link>
                  <button className="right c-btn -info">Add Contact</button>
                </div>
              </form>
            )} />
        </div>
      </div>
    );
  }

  handleMobileNumber(state) {
    var data = fixNumber(state);        
    state.number = data.number;
    state.error = data;
  }

  _onSubmitHandle(state) {
    return (evt) => {
      evt.preventDefault();
      evt.stopPropagation && evt.stopPropagation();
        
      if (state.error === true) {
        alert('Please fix number');
        return;
      }
 
      const { directoryId } = this.props.params;

      ContactActions.store(directoryId, state)
        .then((/*res*/) => {
          alert('The contact was successfully created!');
          this.context.router.transitionTo(`/directories/${directoryId}`);
        });
    }
  }
}

export default DirectoriesShowContactsCreate;
