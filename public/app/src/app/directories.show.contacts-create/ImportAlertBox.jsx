import React, { PropTypes } from 'react';
import { Link } from 'react-router';

export default class ImportAlertBox extends React.Component {
  static propTypes = {
    directoryId: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="c-alert -info g-bottom-space">
        Want to subscribe more than one person at a time?&nbsp;
        <Link to={`/directories/${this.props.directoryId}/contacts/import`}>Import a List</Link>.
      </div>
    );
  }
}
