import React from 'react';

import DirectoryStore from '../../../stores/DirectoryStore';
import ListMenuSelect from './ListMenuSelect';
import ListMenuDeleteButton from './ListMenuDeleteButton';
import ListMenuSortButton from './ListMenuSortButton';

export default class ListMenu extends React.Component {
  render() {
    const empty = !DirectoryStore.isAnySelected();

    return (
      <div className="sortmenu g-row">
        <section className="g-col-1">
          <ListMenuSelect />
        </section>

        <section className="g-col-4">
          <ListMenuSortButton />
        </section>

        <section className="g-col-2">
          { !empty ? <ListMenuDeleteButton /> : null }
        </section>
      </div>
    );
  }
}
