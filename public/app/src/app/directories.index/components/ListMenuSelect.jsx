import React from 'react';

import DirectoryActions from '../../../actions/DirectoryActions';
import DirectoryStore from '../../../stores/DirectoryStore';

export default class ListItemSelect extends React.Component {
  constructor(props) {
    super(props);

    this._onChangeHandle = ::this._onChangeHandle;
  }

  render() {
    return (
      <label className="form-checkbox">
        <input type="checkbox"
          checked={DirectoryStore.isAllSelected()}
          onChange={this._onChangeHandle} />
        <div />
      </label>
    );
  }

  _onChangeHandle() {
    DirectoryActions.selectAll();
  }
}
