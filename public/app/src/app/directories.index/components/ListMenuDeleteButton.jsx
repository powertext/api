import React from 'react';
import DirectoryActions from '../../../actions/DirectoryActions';

export default class ListMenuDeleteButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button type="button" className="c-btn -danger" onClick={::this._onClickHandle}>
        <i className="fa fa-trash pre-icon" />
        Delete
      </button>
    );
  }

  _onClickHandle() {
    DirectoryActions.deleteSelected();
  }
}
