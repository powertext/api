import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';

import ListItemSelect from './ListItemSelect';

export default class ListItem extends React.Component {
  static propTypes = {
    /**
     * Directory data
     * @type Immutable
     */
    directory: React.PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { directory } = this.props;
    const id = directory.get('id');

    return (
      <div className="listitem">
        <div className="info">
          <div className="g-col-1">
          </div>

          <div className="g-col-1">
            <ListItemSelect id={id} />
          </div>

          <div className="g-col-6">
            <h1><Link to={`directories/${id}`}>{directory.get('name')}</Link></h1>
            <h4>Created {moment(directory.get('created_at')).format('LL')}</h4>
          </div>

          <div className="g-col-4">
            <h1>{directory.get('contact_count')}</h1>
            <h4>Contact(s)</h4>
          </div>
        </div>

        <div className="actions">
          <Link to={`directories/${id}`} className="c-btn">View Contacts</Link>
          <Link to={`directories/${id}/contacts/add`} className="c-btn">+</Link>
        </div>
      </div>
    );
  }
}
