import React from 'react';

import DirectoryActions from '../../../actions/DirectoryActions';
import DirectoryStore from '../../../stores/DirectoryStore';

export default class ListMenuSortButton extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <button onClick={::this._onClickHandle} className="c-btn -info">
        {this.renderText()}
      </button>
    );
  }

  renderText() {
    if ( DirectoryStore.isSortAscending() ) {
      return (
        <span className="pre-icon">
          <i className="fa fa-caret-down" />
          Ascending
        </span>
      );
    } else if ( DirectoryStore.isSortDescending() ) {
      return (
        <span className="pre-icon">
          <i className="fa fa-caret-down" />
          Descending
        </span>
      );
    }

    return 'Sort';
  }

  _onClickHandle() {
    DirectoryActions.sort();
  }
}
