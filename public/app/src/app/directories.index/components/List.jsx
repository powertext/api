import React from 'react';

import DirectoryStore from '../../../stores/DirectoryStore';
import ListMenu from './ListMenu';
import ListItem from './ListItem';

export default class List extends React.Component {
  static propTypes = {
    /**
     * List of directories
     * Immutable
     */
    directories: React.PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { directories } = this.props;

    return (
      <div className="c-large-table">
        <ListMenu />

        {directories.map((directory, i) => {
          return <ListItem
            key={i}
            directory={directory} />;
        })}
      </div>
    );
  }
}
