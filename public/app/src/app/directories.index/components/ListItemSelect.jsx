import React, { PropTypes } from 'react';

import DirectoryActions from '../../../actions/DirectoryActions';
import DirectoryStore from '../../../stores/DirectoryStore';

export default class ListItemSelect extends React.Component {
  static propTypes = {
    id: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <label className="form-checkbox">
        <input type="checkbox"
          checked={DirectoryStore.isSelected(this.props.id)}
          onChange={::this._onChangeHandle} />
        <div />
      </label>
    );
  }

  _onChangeHandle() {
    DirectoryActions.select(this.props.id);
  }
}
