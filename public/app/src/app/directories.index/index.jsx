import React from 'react';
import AltContainer from 'alt/AltContainer';
import { Link } from 'react-router';

import DirectoryActions from '../../actions/DirectoryActions';
import DirectoryStore from '../../stores/DirectoryStore';
import List from './components/List';
import ListEmpty from './components/ListEmpty';
import ListLoading from './components/ListLoading';

class DirectoriesIndex extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    DirectoryActions.fetchAll();
  }

  render() {
    return <AltContainer
      store={DirectoryStore}
      render={(props) => {
        const { directories, isFetchingAll, isFetchingAllError } = props;
        const sort = (a, b) => a.get('name') > b.get('name');
        let filteredDirectories = !DirectoryStore.isSortAscending() && !DirectoryStore.isSortDescending()
          ? directories
          : (DirectoryStore.isSortAscending() ? directories.sort(sort) : directories.sort(sort).reverse());
        const list = ( directories == null || directories.size < 1 ? <ListEmpty /> : <List directories={filteredDirectories} />);

        return (
          <div className="directories-index">
            <div className="c-top-menu">
              <div className="left">
                <h1> Directories </h1>
              </div>

              <div className="right">
                <Link to={'directories/create'} className="c-btn -info">New Directory</Link>
              </div>
            </div>

            { isFetchingAll ? <ListLoading /> : list }
          </div>
        );
      }} />;
  }
}

export default DirectoriesIndex;
