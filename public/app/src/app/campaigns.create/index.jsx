import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import AltContainer from 'alt/AltContainer';

import CampaignActions from '../../actions/CampaignActions';
import DirectoryActions from '../../actions/DirectoryActions';
import CampaignStore from '../../stores/CampaignStore';
import DirectoryStore from '../../stores/DirectoryStore';
import SelectedContactStore from '../../stores/SelectedContactStore';
import WizardContent from './components/WizardContent';
import WizardMenu from './components/WizardMenu';
import linkState from '../../utils/linkState';

export default class CampaignsCreate extends React.Component {
  static contextTypes = {
    /**
     * Router instance
     */
    router: PropTypes.object.isRequired
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      step: 0, // active wizard step
      // last step we've been on
      // useful to check if we've filled up
      // the previous step
      last: 0,

      // ..
      message: '',
      name: ''
    };

    DirectoryActions.fetchAll();
  }

  render() {
    const { step, last, ...other } = this.state;

    return <AltContainer
      stores={[DirectoryStore, CampaignStore, SelectedContactStore]}
      inject={{
        directories: () => DirectoryStore.getState().directories,
        selectedDirectoryIds: () => DirectoryStore.getSelectedIds(),
        selectedContacts: () => SelectedContactStore.getState().selected,
        isFetchingDirectoryList: () => DirectoryStore.getState().isFetchingAll,
        isStoring: () => CampaignStore.getState().isStoring,
      }}
      render={({
        directories,
        selectedDirectoryIds,
        isFetchingDirectoryList,
        isStoring,
      }) => (
        <div>
          <div className="breadcrumbs">
            <Link to={'campaigns'}>Campaigns</Link>
            <span>New Campaign</span>
          </div>

          <WizardContent
            directories={directories}
            selectedDirectoryIds={selectedDirectoryIds}
            isFetchingDirectoryList={isFetchingDirectoryList}
            isStoring={isStoring}
            data={other}
            linkState={linkState.bind(null, this)}
            onSubmitHandle={::this._onSubmitHandle}
            step={step} />

          <WizardMenu
            active={step}
            jumpStepHandle={::this._onStepJumpHandle} />
        </div>
      )} />;
  }

  /**
   * Jumps to the provided step
   *
   * @params {int} step
   */
  _onStepJumpHandle(step) {
    const { last } = this.state;

    this.setState({
      step: step,
      last: step > last ? step : last
    });
  }

  _onSubmitHandle(evt) {
    evt.preventDefault();
    evt.stopPropagation && evt.stopPropagation();
    const { step, last, ...other } = this.state;

    CampaignActions.store({
      ...other,
      contacts: SelectedContactStore.getState().selected
    }).then((res) => {
      this.context.router.transitionTo(`/campaigns/${res.data.data.id}`);
    })
    .catch((err) => {
      console.info(err);
    });
  }
}
