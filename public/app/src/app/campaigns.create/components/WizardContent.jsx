import React, { PropTypes } from 'react';

import Selection from './Selection';
import Compose from './Compose';
import Summary from './Summary';

export default class WizardContent extends React.Component {
  static propTypes = {
    /**
     * The current active step
     */
    step: PropTypes.number.isRequired,

    /**
     * linkState binded to the container
     */
    linkState: PropTypes.func.isRequired,

    /**
     * Container state
     */
    data: PropTypes.object.isRequired,

    /**
     * Submit FN
     */
    onSubmitHandle: PropTypes.func.isRequired,

    /**
     * @store DirectoryStore
     */
    directories: PropTypes.object.isRequired,

    /**
     * @store DirectoryStore
     */
    selectedDirectoryIds: PropTypes.arrayOf(
      PropTypes.number
    ).isRequired,

    /**
     * @store DirectoryStore
     */
    isFetchingDirectoryList: PropTypes.bool.isRequired,

    /**
     * @store CampaignStore
     */
    isStoring: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {
      step,
      data,
      linkState,
      onSubmitHandle,
      directories,
      selectedDirectoryIds,
      isStoring,
      isFetchingDirectoryList
    } = this.props;

    switch (step) {
      case 0: return <Selection
        directories={directories}
        isFetchingAll={isFetchingDirectoryList} />;
      case 1: return <Compose
        data={data}
        linkState={linkState} />;
      case 2: return <Summary
        data={data}
        isStoring={isStoring}
        selectedDirectoryIds={selectedDirectoryIds}
        onSubmitHandle={onSubmitHandle} />;
      default: return null;
    }
  }
}
