import React, { PropTypes } from 'react';

import ContactActions from '../../../../actions/ContactActions';
import SelectedContactStore from '../../../../stores/SelectedContactStore';

export default class ItemSelect extends React.Component {
  static propTypes = {
    /**
     * Directory ID
     */
    directory: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <label className="form-checkbox">
        <input type="checkbox"
          checked={SelectedContactStore.isDirectorySelected(this.props.directory)}
          onChange={::this._onChangeHandle} />
        <div />
      </label>
    );
  }

  _onChangeHandle() {
    ContactActions.selectDirectory(this.props.directory);
  }
}
