import React, { PropTypes } from 'react';
import classnames from 'classnames';

import ItemSelect from './ItemSelect';
import ItemContactSelect from './ItemContactSelect';

export default class SelectionItem extends React.Component {
  static propTypes = {
    /**
     * @store DirectoryStore.x
     */
    directory: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      expanded: false
    };
  }

  render() {
    const { expanded } = this.state;
    const { directory } = this.props;

    return (
      <div className={classnames('item', {
        '-expanded': expanded,
        '-closed': !expanded
      })}>
        <section className="g-row">
          <section className="g-col-8 g-row">
            <section className="g-col-1">
              <ItemSelect directory={directory} />
            </section>
            <section className="g-col-11">
              <h2>{directory.get('name')}</h2>
              <h3>({directory.get('contact_count')} Contacts)</h3>
            </section>
          </section>
          <section className="g-col-4">
            <div className="actions">
              <button type="button" className="c-btn" onClick={::this._toggleExpand}>
                <i className={classnames('fa', {
                  'fa-caret-right': !expanded,
                  'fa-caret-down': expanded
                })} />
              </button>
            </div>
          </section>
        </section>

        <div className="c-selection-list">
          {directory.get('contacts').map((contact, i) => (
            <div className="item" key={i}>
              <section className="g-row">
                <section className="g-col-8 g-row">
                  <section className="g-col-1">
                    <ItemContactSelect id={contact.get('id')} />
                  </section>
                  <section className="g-col-11">
                    <h3>{contact.get('first_name')} {contact.get('last_name')}</h3>
                  </section>
                </section>
              </section>
            </div>
          ))}
        </div>
      </div>
    );
  }

  /**
   * Toggle
   */
  _toggleExpand(status) {
    this.setState({
      expanded: !this.state.expanded
    });
  }
}
