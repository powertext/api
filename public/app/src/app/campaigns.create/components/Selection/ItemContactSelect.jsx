import React, { PropTypes } from 'react';

import DirectoryActions from '../../../../actions/DirectoryActions';
import SelectedContactStore from '../../../../stores/SelectedContactStore';

export default class ItemContactSelect extends React.Component {
  static propTypes = {
    /**
     * Directory ID
     */
    id: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <label className="form-checkbox">
        <input type="checkbox"
          checked={SelectedContactStore.isContactSelected(this.props.id)}
          onChange={::this._onChangeHandle} />
        <div />
      </label>
    );
  }

  _onChangeHandle() {
    ContactActions.selectContact(this.props.id);
  }
}
