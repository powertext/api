import React, { PropTypes } from 'react';
import classnames from 'classnames';

import CampaignsMessageCharacterCountLabel from '../../../../components/CampaignsMessageCharacterCountLabel';
import CampaignsMessagePrice from '../../../../components/CampaignsMessagePrice';
import linkState from '../../../../utils/linkState';
import CampaignTermsModal from '../../../../components/CampaignTermsModal';

export default class Summary extends React.Component {
  static propTypes = {
    /**
     * Submit FN
     */
    onSubmitHandle: PropTypes.func.isRequired,

    /**
     * @store CampaignStore
     */
    isStoring: PropTypes.bool.isRequired,

    /**
     * @store CampaignStore
     */
    data: PropTypes.object.isRequired,

    /**
     * @param DirectoryStore
     */
    selectedDirectoryIds: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = { disclaimer: false };
  }

  render() {
    const { disclaimer } = this.state;
    const {
      data,
      onSubmitHandle,
      isStoring,
      selectedDirectoryIds
    } = this.props;


    return (
      <section>
        <div className="c-top-menu">
          <div className="left">
            <h1>Summary</h1>
          </div>
        </div>

        <section className="g-row g-bottom-space-narrow c-formatted-content">
          <section className="g-col-6">
            <p className="white-space-pre">
            {data.message}
            </p>
          </section>

          <section className="g-col-6">
            <h2>Number of Recipients: <strong>0</strong> Contacts selected in <strong>{selectedDirectoryIds.size}</strong> Directories</h2>
            <CampaignsMessageCharacterCountLabel message={data.message} />
            <CampaignsMessagePrice message={data.message} />
          </section>
        </section>

        <div className="form-container modal">
          <label>
            <input
              type="checkbox"
              name="disclaimer"
              checkedLink={linkState(this, 'disclaimer')} />
              <strong>You agree to the <a onClick={::this.openTerms}>terms and conditions</a> before sending this message.</strong>
          </label>
          <CampaignTermsModal ref='terms_modal'  />
        </div>

        <button onClick={onSubmitHandle}
          disabled={isStoring}
          className={classnames('c-btn -success', {
            '-hidden': !disclaimer
          })}>
          Send Now
        </button>
      </section>
    );
  }

  openTerms(evt) {
    evt.stopPropagation();
    this.refs.terms_modal.open();  
  }
}
