import React from 'react';
import { Link } from 'react-router';

export default class AuthForgotPassword extends React.Component {
  render() {
    return (
      <div className="c-isolated-section">
        <div className="outercontainer">
          <div className="innercontainer">
            <h2> Forgot Password </h2>
            <form onSubmit={::this._onSubmitHandle}>
              <div className="form-container">
                <input type="text" name="text" className="form-control" placeholder="Enter your email address" />
              </div>
              <button type="submit" className="submitbtn">Submit Reset Link</button>
            </form>
            <Link to={'/login'}>&larr; No, I'd like to login</Link>
          </div>
        </div>
      </div>
    );
  }

  _onSubmitHandle(evt) {
    evt.preventDefault();
    evt.stopPropagation && evt.stopPropagation();
  }
}
