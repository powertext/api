import React from 'react';
import { Link } from 'react-router';

import linkState from '../../utils/linkState';
import DirectoryActions from '../../actions/DirectoryActions';
import State from '../../components/State';

export default class DirectoriesShowSettings extends React.Component {
  static propTypes = {
    /**
     * Directory data
     * @type Immutable
     */
    directory: React.PropTypes.object.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { directory } = this.props;

    return (
      <div>
        <div className="c-top-menu -no-allowance-top -narrow-allowance-bottom">
          <div className="left">
            <h1>Settings</h1>
          </div>
        </div>

        <State
          init={{ name: directory.get('name') }}
          render={(state, ctx) => {
            return (
              <form onSubmit={::this._onSubmitHandle(state)}>
                <div className="form-container g-wide">
                  <label htmlFor="directory-name">Directory Name</label>
                  <input type="text"
                    className="form-control"
                    id="directory-name"
                    name="name"
                    valueLink={linkState(ctx, 'name')} />
                </div>

                <button className="c-btn -info">Update</button>
              </form>
            );
          }} />
      </div>
    );
  }

  _onSubmitHandle(state) {
    return (evt) => {
      evt.preventDefault();
      evt.stopPropagation && evt.stopPropagation();
      const { directoryId } = this.props;

      DirectoryActions.update(directoryId, state)
        .then(() => {
          alert('Settings have been updated!');
          this.context.router.transitionTo(`/directories/${directoryId}`);
        });
    };
  }
}
