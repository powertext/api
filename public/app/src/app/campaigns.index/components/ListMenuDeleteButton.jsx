import React from 'react';

import CampaignActions from '../../../actions/CampaignActions';
import CampaignStore from '../../../stores/CampaignStore';

export default class ListMenuDeleteButton extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    // needs REFACTOR LOL
    // make this component a dumb component
    const empty = !CampaignStore.isAnySelected();

    return !empty ? (
      <button type="button" className="c-btn -danger" onClick={::this._onClickHandle}>
        <i className="fa fa-trash pre-icon" />
        Delete
      </button>
    ) : null;
  }

  _onClickHandle() {
    CampaignActions.deleteSelected();
  }
}
