import React, { PropTypes } from 'react';

export default class ListItemDeleteButton extends React.Component {
  static propTypes = {
    /**
     * Campaign ID
     */
    id: PropTypes.number.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <a href="javascript:void(0);">
        Delete
      </a>
    );
  }

  _onClickHandle(evt) {
    evt.preventDefault();
    evt.stopPropagation && evt.stopPropagation();

    // do something
    // CampaignActions.delete(this.props.id);
  }
}
