import React from 'react';

import ListMenuSelect from './ListMenuSelect';
import ListMenuSortButton from './ListMenuSortButton';
import ListMenuDeleteButton from './ListMenuDeleteButton';

export default class ListMenu extends React.Component {
  render() {
    return (
      <div className="sortmenu g-row">
        <section className="g-col-1">
          <ListMenuSelect />
        </section>

        <section className="g-col-4">
          <ListMenuSortButton />
        </section>

        <section className="g-col-2">
          <ListMenuDeleteButton />
        </section>
      </div>
    );
  }
}
