import React, { PropTypes } from 'react';

import CampaignActions from '../../../actions/CampaignActions';
import CampaignStore from '../../../stores/CampaignStore';

export default class ListMenuSelect extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <label className="form-checkbox">
        <input type="checkbox"
          checked={CampaignStore.isAllSelected()}
          onChange={::this._onChangeHandle} />
        <div />
      </label>
    );
  }

  _onChangeHandle() {
    CampaignActions.selectAll();
  }
}
