import React from 'react';
import { Link } from 'react-router';

export default class ListEmpty extends React.Component {
  render() {
    return (
      <section>
        <h1>There are no campaigns yet!</h1>
        <Link to={'/campaigns/create'}>Why don't you create one?</Link>
      </section>
    );
  }
}
