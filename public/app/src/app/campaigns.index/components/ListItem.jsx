import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';
import moment from 'moment';

import ListItemSelect from './ListItemSelect';
import CampaignsStatus from '../../../components/CampaignsStatus';

export default class ListItem extends React.Component {
  static propTypes = {
    /**
     * @store CampaignStore [.campaigns.campaign]
     * @type ImmutableMap
     */
    campaign: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { campaign } = this.props;

    return (
      <div className="listitem">
        <div className="info">
          <div className="g-col-1">
          </div>

          <div className="g-col-1">
            <ListItemSelect id={campaign.get('id')} />
          </div>

          <div className="g-col-6">
            <h1><Link to={`/campaigns/${campaign.get('id')}/edit`}>{campaign.get('name')}</Link></h1>
            <h4>Edit {moment(campaign.get('updated_at')).format('MMMM Do YYYY, h:mm:ss a')}</h4>
          </div>

          <div className="g-col-2">
            <CampaignsStatus status={campaign.get('is_sent')} />
          </div>

          <div className="g-col-2">
            { /*? <span className="c-label -small -info">Report</span> : null*/ }
          </div>
        </div>

        <div className="actions">
          <div className="c-dropdown">
            <button className="c-btn" type="button">
              <i className="fa fa-caret-down" />
            </button>
            <ul className="dropdown-menu -right
            ">
              <li><a href="#">Duplicate</a></li>
              <li><a href="#">Delete</a></li>
            </ul>
          </div>

          <Link to={`/campaigns/${campaign.get('id')}`} className="c-btn">
            <i className="fa fa-pencil pre-icon" /> Edit
          </Link>
        </div>
      </div>
    );
  }
}
