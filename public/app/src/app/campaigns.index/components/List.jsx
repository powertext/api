import React, { PropTypes } from 'react';

import ListLoading from './ListLoading';
import ListEmpty from './ListEmpty';
import ListMenu from './ListMenu';
import ListItem from './ListItem';

export default class List extends React.Component {
  static propTypes = {
    /**
     * @store CampaignStore
     * @type Immutable.List
     */
    campaigns: PropTypes.object.isRequired,

    /**
     * @store CampaignStore
     */
    isFetchingAll: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { campaigns, isFetchingAll } = this.props;

    console.log(campaigns);

    if ( isFetchingAll ) {
      return <ListLoading />;
    }

    return !campaigns.size
      ? <ListEmpty />
      : (
        <div className="c-large-table">
          <ListMenu />
          {this.renderItems()}
        </div>
      );
  }

  renderItems() {
    return this.props.campaigns.map((campaign, i) => (
      <ListItem campaign={campaign} key={i} />
    ));
  }
}
