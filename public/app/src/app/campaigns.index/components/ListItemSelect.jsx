import React, { PropTypes } from 'react';

import CampaignActions from '../../../actions/CampaignActions';
import CampaignStore from '../../../stores/CampaignStore';

export default class ListItemSelect extends React.Component {
  static propTypes = {
    /**
     * Campaign ID
     */
    id: PropTypes.number.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <label className="form-checkbox">
        <input type="checkbox"
          checked={CampaignStore.isSelected(this.props.id)}
          onChange={::this._onChangeHandle} />
        <div />
      </label>
    );
  }

  _onChangeHandle() {
    CampaignActions.select(this.props.id);
  }
}
