import React from 'react';

import CampaignActions from '../../../actions/CampaignActions';
import CampaignStore from '../../../stores/CampaignStore';

export default class ListMenuSortButton extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <button onClick={::this._onClickHandle} className="c-btn -info">
        {this.renderText()}
      </button>
    );
  }

  renderText() {
    if ( CampaignStore.isSortAscending() ) {
      return (
        <span className="pre-icon">
          <i className="fa fa-caret-down" />
          Ascending
        </span>
      );
    } else if ( CampaignStore.isSortDescending() ) {
      return (
        <span className="pre-icon">
          <i className="fa fa-caret-down" />
          Descending
        </span>
      );
    }

    return 'Sort';
  }

  _onClickHandle() {
    CampaignActions.sort();
  }
}
