import AltContainer from 'alt/AltContainer';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import CampaignActions from '../../actions/CampaignActions';
import CampaignStore from '../../stores/CampaignStore';
import List from './components/List';

export default class CampaignsIndex extends React.Component {
  static propTypes = {
    // Router params
    params: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    CampaignActions.fetchAll();
  }

  render() {
    return <AltContainer
      store={CampaignStore}
      render={({ campaigns, isFetchingAll }) => {
        const sort = (a, b) => a.get('name') > b.get('name');
        let filteredCampaigns = !CampaignStore.isSortAscending() && !CampaignStore.isSortDescending()
          ? campaigns
          : (CampaignStore.isSortAscending() ? campaigns.sort(sort) : campaigns.sort(sort).reverse());

        return (
          <div>
            <div className="c-top-menu">
              <div className="left">
                <h1>Campaigns</h1>
              </div>

              <div className="right">
                <Link to={'campaigns/create'} className="c-btn -info">New Campaign</Link>
              </div>
            </div>

            <List campaigns={filteredCampaigns} isFetchingAll={isFetchingAll} />
          </div>
        );
      }} />;
  }
}
