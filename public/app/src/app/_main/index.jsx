import React, { PropTypes} from 'react';
import Navigation from '../../templates/Navigation';
import SessionActions from '../../actions/SessionActions';
import connectToStores from 'alt/utils/connectToStores';
import SessionStore from '../../stores/SessionStore';

/**
 * Main (app) base template
 */
@connectToStores
export default class Main extends React.Component {

  static contextTypes = {
    router: PropTypes.object.isRequired
  }

  static getStores() {
    return [SessionStore];
  }

  static getPropsFromStores() {
  //  console.log(SessionStore);
    return SessionStore.getState();
  }
  
  constructor(props, context) {
    super(props, context);
    
  }

  componentWillMount() {
   if (this.props.auth == null) {
        this.context.router.transitionTo('login');
   }
  }

  render() { 
    const {auth} = this.props;
 
    return (
      <div>
        {auth && auth.data ? <Navigation {...this.props} />: ''} 
       
        <div className="g-container">
          {this.props.children}
        </div>
      </div>
    );
  }
}
