import React from 'react';
import { Link } from 'react-router';
import SessionActions from '../../actions/SessionActions';
import SessionStore from '../../stores/SessionStore';
import connectToStores from 'alt/utils/connectToStores';

@connectToStores
export default class AuthLogin extends React.Component {

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  constructor(props, context) {
      super(props, context);
      SessionStore.onChange(::this._onAuthChange);
  }

  static getStores() {
      return [SessionStore];
  }

  static getPropsFromStores() {
      return SessionStore.getState();
  }
    
  render() {
    return (
      <div className="c-isolated-section">
        <div className="outercontainer">
          <div className="innercontainer">
            <h2> Powertext </h2>
            <form onSubmit={::this._onSubmitHandle}>
              <div className="form-container-wrapped">
                <input onChange={::this._onEmailChangeHandle}  type="text" name="text" className="form-control" placeholder="my@email.com" />
                <input onChange={::this._onPasswordChangeHandle}  type="password" name="password" className="form-control" placeholder="*****" />
              </div>
              <button type="submit" className="submitbtn">Sign In</button>
            </form>
            <Link to={'/forgot-password'}>Forgot Password</Link>
          </div>
        </div>
      </div>
    );
  }

  _onAuthChange(state) {
    if (typeof(state) !== 'string') {
        this.context.router.transitionTo('/directories');
    }
  }

  _onSubmitHandle(evt) {
    evt.preventDefault();
    evt.stopPropagation && evt.stopPropagation();
    SessionActions.login(this.state);

  }


  _onEmailChangeHandle(evt) {
    this.setState({
        email: evt.target.value
    });  
  }

  _onPasswordChangeHandle(evt) {
    this.setState({
        password: evt.target.value
    }); 
  }
}
