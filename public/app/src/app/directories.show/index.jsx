import React, { PropTypes } from 'react';
import AltContainer from 'alt/AltContainer';
import cloneWithProps from 'react/lib/cloneWithProps';
import { Link } from 'react-router';
import { resolve } from 'react-resolver';

import DirectoryActions from '../../actions/DirectoryActions';
import DirectoryStore from '../../stores/DirectoryStore';
import ContactStore from '../../stores/ContactStore';
import ContactActions from '../../actions/ContactActions';
import DirectoriesShowContacts from '../directories.show.contacts';
 
@resolve({
    directory(props) {
      const id = props.params.directoryId;
      return DirectoryActions.fetch(id);
    },

    children(props) {
      return props.children;
    },

    params(props) {
      return props.params;
    }  
})
export default class DirectoriesShow extends React.Component {

  static displayName = 'DirectoriesShow';

  static contextTypes = {
    /**
     * Router instance
     */
    router: PropTypes.object.isRequired
  };

  static propTypes = {
    /**
     * Route parameters
     */
    params: PropTypes.object.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    const id = this.props.params.directoryId;
    ContactActions.fetchAll(id);
  }

  render() {
    const { children, params } = this.props;
    const { directoryId } = params;

    return <AltContainer
      stores={[DirectoryStore, ContactStore]}
      inject={{
        directory: () => DirectoryStore.getState().directory,
        contacts: () => ContactStore.getState().contacts
      }}
      render={({ directory, contacts }) => {
        const props = {
          directory,
          contacts,
          ...params
        };

        return (
          <section>
            <div className="breadcrumbs">
              <Link to={'/directories'}>Directories</Link>
              <span>{directory.get('name')}</span>
            </div>

            <div className="c-top-menu">
              <div className="left">
                <h2>{directory.get('name')}</h2>
                <h2><Link to={`/directories/${directoryId}`} className="contactcount">{contacts.size}</Link></h2>
              </div>
            </div>

            <div className="c-simple-tabs">
              <div className="tablist">
                <Link to={`/directories/${directoryId}`} activeClassName="-active" className="tab">Contacts</Link>
                <Link to={`/directories/${directoryId}/settings`} activeClassName="-active" className="tab">Settings</Link>
              </div>

              <div className="content">
                { children == null
                  ? <DirectoriesShowContacts {...props} />
                  : cloneWithProps(children, props)
                }
              </div>
            </div>
          </section>
        );
      }} />;
  }
}


/**export default Resolver.createContainer(DirectoriesShow, {
  resolve: {
    directory(props) {
      const id = props.params.directoryId;
      return DirectoryActions.fetch(id);
    },

    children(props) {
      return props.children;
    },

    params(props) {
      return props.params;
    }
  }
});**/
