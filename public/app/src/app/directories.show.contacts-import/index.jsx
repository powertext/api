import React from 'react';
import AltContainer from 'alt/AltContainer';

import ContactImportActions from './ContactImportActions';
import ContactImportStore from './ContactImportStore';
import Step1 from './Step1';
import Step2 from './Step2';

export default class DirectoriesShowContactsImport extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { directoryId } = this.props.params;

    return <AltContainer
      store={ContactImportStore}
      render={(props) => {
        const {
          step,
          file,
          isImporting,
          isImportingError,
          columns,
          parsedColumns,
          parsedFile,
          isProcessing,
          isProcessingError
        } = props;

        return step === 0
        ? (<Step1
          file={file}
          directoryId={directoryId}
          isImporting={isImporting}
          isImportingError={isImportingError}
          importFile={::ContactImportActions.importFile}
          selectFile={::ContactImportActions.selectFile} />
        ) : (<Step2
          directoryId={directoryId}
          columns={columns}
          parsedFile={parsedFile}
          parsedColumns={parsedColumns}
          isProcessing={isProcessing}
          isProcessingError={isProcessingError}
          processColumns={::ContactImportActions.processColumns}
          setColumnIndex={::ContactImportActions.setColumnIndex} />
        );
      }} />;
  }
}
