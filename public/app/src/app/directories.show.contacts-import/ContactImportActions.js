import { createActions } from 'alt/utils/decorators';

import alt from '../../alt';
import generateActions from '../../utils/alt/generateActions';
import ContactsAPIUtil from '../../utils/request/contacts';

@createActions(alt)
@generateActions('processColumns', 'importFile')
class ContactImportActions {
  constructor() {
    this.generateActions(
      'setColumnIndex'
    );
  }

  selectFile(file) {
    this.dispatch(file);
  }

  processColumns(id, importId, data) {
    return ContactsAPIUtil.processColumns(id, importId, data)
      .then(::this.actions.processColumnsSuccess)
      .catch(::this.actions.processColumnsError);
  }

  importFile(id, data) {
    return ContactsAPIUtil.importFile(id, data)
      .then(::this.actions.importFileSuccess)
      .catch(::this.actions.importFileError);
  }
}

export default ContactImportActions;
