import { createStore } from 'alt/utils/decorators';

import alt from '../../alt';
import ContactImportActions from './ContactImportActions';

@createStore(alt)
class ContactImportStore {
  static config = {
    stateKey: 'state'
  };

  static displayName = 'ContactImportStore';

  constructor() {
    this.state = {
      step: 0,
      file: null,
      // importing file
      isImporting: false,
      isImportingError: false,
      // column (set) data
      columns: {
        first_name: 0,
        last_name: 1,
        number: 2
      },
      // parsed columns
      parsedColumns: [],
      parsedFile: '',
      // processing columns
      isProcessing: false,
      isProcessingFailed: false
    };

    this.bindActions(ContactImportActions);
    this.exportPublicMethods({ isSelected: ::this.isSelected });
  }

  isSelected(index) {
    const { parsedColumns } = this.state;

    for ( key in  parsedColumns ) {
      if ( parsedColumns[key] === index ) {
        return true;
      }
    }

    return false;
  }

  onSelectFile(file) {
    this.setState({ file });
  }

  onImportFile() {
    this.setState({
      isImporting: true,
      isImportingError: false
    });
  }

  onImportFileSuccess(data) {
    this.setState({
      step: 1,
      parsedFile: data.id,
      parsedColumns: data.columns,
      isImporting: false,
      isImportingError: false
    });
  }

  onImportFileError(err) {
    this.setState({
      isImporting: false,
      isImportingError: err
    });
  }

  onProcessColumns() {
    this.setState({
      isProcessing: true,
      isProcessingFailed: false
    });
  }

  onProcessColumnsSuccess() {
    this.setState({ isProcessing: false });
  }

  onProcessColumnsError(err) {
    this.setState({
      isProcessing: false,
      isProcessingFailed: err
    });
  }

  onSetColumnIndex({ column, index }) {
    this.setState({
      columns: {
        ...this.state.columns,
        [column]: index
      }
    });
  }
}

export default ContactImportStore;
