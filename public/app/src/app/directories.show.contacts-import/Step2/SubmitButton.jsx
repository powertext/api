import React, { PropTypes } from 'react';

export default class SubmitButton extends React.Component {
  static propTypes = {
    columns: PropTypes.object.isRequired,
    directoryId: PropTypes.number.isRequired,
    isProcessing: PropTypes.bool.isRequired,
    processColumns: PropTypes.func.isRequired,
    parsedFile: PropTypes.string.isRequired,
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { isProcessing } = this.props;

    return (
      <button
        type="button"
        className="c-btn -info"
        disabled={isProcessing}
        onClick={::this._onClickHandle}>
        Process Columns
      </button>
    );
  }

  _onClickHandle(evt) {
    const { directoryId, parsedFile, columns } = this.props;
    this.props.processColumns(
      directoryId,
      parsedFile,
      { ...columns }
    ).then(() => {
      alert('Successfully imported contacts');
      window.location = `#/directories/${directoryId}`;
    });
  }
}
