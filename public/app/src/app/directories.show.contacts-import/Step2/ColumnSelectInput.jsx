import React, { PropTypes } from 'react';

export default class ColumnSelectInput extends React.Component {
  static propTypes = {
    column(props, propName) {
      const value = props[propName];
      const columns = ['first_name', 'last_name', 'number'];

      if ( columns.indexOf(value) === -1 ) {
        throw new Error(`<${value}> must be one of [${columns.join(', ')}]`);
      }
    },
    columns: PropTypes.object.isRequired,
    parsedColumns: PropTypes.array.isRequired,
    setColumnIndex: PropTypes.func.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { column, columns, parsedColumns } = this.props;

    return (
      <select
        className="form-control"
        defaultValue={columns[column]}
        onChange={::this._onChangeHandle}>
        {parsedColumns.map((column, i) => (
          <option value={i} key={i}>{column}</option>
        ))}
      </select>
    );
  }

  _onChangeHandle(evt) {
    this.props.setColumnIndex(
      this.props.column,
      evt.target.value
    );
  }
}
