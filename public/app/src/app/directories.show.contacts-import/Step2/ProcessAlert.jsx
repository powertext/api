import React, { PropTypes } from 'react';

export default class ProcessAlert extends React.Component {
  static propTypes = {
    isProcessingError: PropTypes.bool.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return this.props.isProcessingError ? (
      <div className="c-alert -danger g-bottom-space">
        An error occured!
      </div>
    ) : null;
  }
}
