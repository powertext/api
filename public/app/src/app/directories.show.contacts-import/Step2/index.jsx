import React, { PropTypes } from 'react';

import ProcessAlert from './ProcessAlert';
import ColumnSelectInput from './ColumnSelectInput';
import SubmitButton from './SubmitButton';

export default class Step2 extends React.Component {
  static propTypes = {
    columns: PropTypes.object.isRequired,
    directoryId: PropTypes.number.isRequired,
    parsedFile: PropTypes.string.isRequired,
    parsedColumns: PropTypes.array.isRequired,
    isProcessing: PropTypes.bool.isRequired,
    isProcessingError: PropTypes.bool.isRequired,
    processColumns: PropTypes.func.isRequired,
    setColumnIndex: PropTypes.func.isRequired,
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {
      columns,
      directoryId,
      parsedColumns,
      parsedFile,
      isProcessing,
      isProcessingError,
      setColumnIndex,
      processColumns
    } = this.props;

    return (
      <section>
        <ProcessAlert isProcessingError={isProcessingError} />

        <section className="g-row g-bottom-space">
          <section className="g-col-4">
            <label><h1>First Name</h1></label>
            <ColumnSelectInput
              column={'first_name'}
              columns={columns}
              parsedColumns={parsedColumns}
              setColumnIndex={setColumnIndex} />
          </section>

          <section className="g-col-4">
            <label><h1>Last Name</h1></label>
            <ColumnSelectInput
              column={'last_name'}
              columns={columns}
              parsedColumns={parsedColumns}
              setColumnIndex={setColumnIndex} />
          </section>

          <section className="g-col-4">
            <label><h1>Number</h1></label>
            <ColumnSelectInput
              column={'number'}
              columns={columns}
              parsedColumns={parsedColumns}
              setColumnIndex={setColumnIndex} />
          </section>
        </section>

        <SubmitButton
          columns={columns}
          directoryId={directoryId}
          isProcessing={isProcessing}
          parsedFile={parsedFile}
          processColumns={processColumns} />
      </section>
    );
  }
}
