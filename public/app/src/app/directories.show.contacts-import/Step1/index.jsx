import React, { PropTypes } from 'react';
import ImportAlert from './ImportAlert';

export default class Step1 extends React.Component {
  static propTypes = {
    file: PropTypes.object,
    directoryId: PropTypes.number.isRequired,
    isImporting: PropTypes.bool.isRequired,
    isImportingError: PropTypes.bool.isRequired,
    selectFile: PropTypes.func.isRequired,
    importFile: PropTypes.func.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { file, isImporting, isImportingError } = this.props;

    console.log(file, isImporting);

    return (
      <section>
        <div className="c-top-menu -no-allowance-top">
          <div className="left">
            <h1>Import from CSV file</h1>
          </div>
        </div>

        <ImportAlert isImportingError={isImportingError} />

        <div className="g-wide">
          <form onSubmit={::this.handleSubmit}>
            <div className="form-container">
              <label htmlFor="import-contact-csv">Upload a file</label>
              <input type="file"
                className="form-control"
                id="import-contact-csv"
                name="csv"
                accept="text/csv"
                onChange={::this.handleInputChange} />
              <p className="tip">Accepted file types: CSV</p>
            </div>

            <button className="c-btn -info" id='import_submit_btn' disabled={file == null || isImporting}>
             {isImporting && <span class="fa fa-spin fa-circle-o-notch" /> || <span>Import Contacts</span>}
            </button>
          </form>
        </div>
      </section>
    );
  }

  handleSubmit(evt) {
    evt.preventDefault();
    evt.stopPropagation && evt.stopPropagation();
    const { file, directoryId } = this.props;
    const form = new FormData();

    if ( !file ) {
      return;
    }

    form.append('file', file);
    this.props.importFile(directoryId, form);
  }

  handleInputChange(evt) {
    this.props.selectFile(evt.target.files[0]);
  }
}
