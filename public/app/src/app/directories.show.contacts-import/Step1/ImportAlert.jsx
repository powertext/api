import React, { PropTypes } from 'react';

export default class ImportAlert extends React.Component {
  static propTypes = {
    isImportingError: PropTypes.bool.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return this.props.isImportingError ? (
      <div className="c-alert -danger g-bottom-space">
        An error occured.
      </div>
    ) : null;
  }
}
