import React, { PropTypes } from 'react';
import AltContainer from 'alt/AltContainer';

import DirectoryStore from '../../stores/DirectoryStore';
import CampaignStore from '../../stores/CampaignStore';
import DirectoryQuickCreate from './components/DirectoryQuickCreate';
import CampaignQuickCreate from './components/CampaignQuickCreate';

export default class Home extends React.Component {
  render() {
    return <AltContainer
      stores={[DirectoryStore, CampaignStore]}
      inject={{
        directories: () => DirectoryStore.getState().directories,
        selectedDirectoryIds: () => DirectoryStore.getSelectedIds(),
        isCampaignStoring: () => CampaignStore.getState().isStoring,
        isDirectoryListFetching: () => DirectoryStore.getState().isFetchingAll,
        isDirectoryStoring: () => DirectoryStore.getState().isStoring
      }}
      render={({
        directories,
        selectedDirectoryIds,
        isCampaignStoring,
        isDirectoryListFetching,
        isDirectoryStoring
      }) => {
        return (
          <section>
            <div className="c-top-menu">
              <div className="left">
                <h1>Home</h1>
              </div>
            </div>

            <section className="g-row">
              <DirectoryQuickCreate isStoring={isDirectoryStoring} />

              <CampaignQuickCreate
                directories={directories}
                selectedDirectoryIds={selectedDirectoryIds}
                isStoring={isCampaignStoring}
                isDirectoryListFetching={isDirectoryListFetching} />
            </section>
        </section>
      );
    }} />;
  }
}
