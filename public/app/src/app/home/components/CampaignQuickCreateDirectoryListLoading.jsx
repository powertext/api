import React from 'react';

export default class CampaignQuickCreateDirectoryListLoading extends React.Component {
  render() {
    return (
      <div className="c-notice-section">
        <div className="innersection">
          <h1> Loading.. </h1>
          <h3> All directories are being loaded.. </h3>
        </div>
      </div>
    );
  }
}
