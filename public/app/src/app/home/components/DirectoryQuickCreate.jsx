import React, { PropTypes } from 'react';

import DirectoryActions from '../../../actions/DirectoryActions';
import DirectoryStore from '../../../stores/DirectoryStore';
import State from '../../../components/State';
import linkState from '../../../utils/linkState';

export default class DirectoryQuickCreate extends React.Component {
  static propTypes = {
    /**
     * @store DirectoryStore
     * @state isStoring
     */
    isStoring: PropTypes.bool.isRequired
  };

    state = {
      name: ''
    }

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="g-col-6 c-panel">
        <h1> Quick Create Directory </h1>
        <div className="typo-hr" />
        <form onSubmit={::this._onSubmitHandle}>
          <section className="form-container">
            <label htmlFor="quick-create-directory-name"> Directory Name </label>
            <input
              type="text"
              className="form-control"
              id="quick-create-directory-name"
              valueLink={linkState(this, 'name')} />
          </section>

          <button className="c-btn -info" disabled={this.props.isStoring}>
            Create Directory
          </button>
        </form>
      </div>
    );
  }

  _onSubmitHandle(evt) {
    evt.preventDefault();
    evt.stopPropagation && evt.stopPropagation();

    if ( this.props.isStoring ) {
      return;
    }

    DirectoryActions.store(this.state);
    this.setState({ name: '' });
  }
}
