import React, { PropTypes } from 'react';

import DirectoryActions from '../../../actions/DirectoryActions';
import DirectoryStore from '../../../stores/DirectoryStore';

export default class CampaignQuickCreateDirectoryListSelect extends React.Component {
  static propTypes = {
    // Directory ID
    id: PropTypes.number.isRequired
  };

  render() {
    return (
      <label className="form-checkbox">
        <input type="checkbox"
          checked={DirectoryStore.isSelected(this.props.id)}
          onChange={::this._onChangeHandle} />
        <div />
      </label>
    );
  }

  _onChangeHandle() {
    DirectoryActions.select(this.props.id)
  }
}
