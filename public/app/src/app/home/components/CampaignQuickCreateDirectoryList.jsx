import React, { PropTypes } from 'react';

import DirectoryActions from '../../../actions/DirectoryActions';
import CampaignQuickCreateDirectoryListSelect from './CampaignQuickCreateDirectoryListSelect';
import CampaignQuickCreateDirectoryListEmpty from './CampaignQuickCreateDirectoryListEmpty';
import CampaignQuickCreateDirectoryListLoading from './CampaignQuickCreateDirectoryListLoading';

export default class CampaignQuickCreateDirectoryList extends React.Component {
  static propTypes = {
    /**
     * @store DirectoryStore
     * @state directories
     * @type Immutable
     */
    directories: PropTypes.object.isRequired,

    /**
     * @store DirectoryStore
     * @state isFetchingAll
     */
    isFetchingAll: PropTypes.bool.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    DirectoryActions.fetchAll();
  }

  render() {
    const { directories, isFetchingAll } = this.props;

    return (
      <div className="c-scroll-section g-bottom-space">
        { isFetchingAll
          ? <CampaignQuickCreateDirectoryListLoading />
          : (!directories.size ? <CampaignQuickCreateDirectoryListEmpty /> : directories.map(::this.renderItem))
        }
      </div>
    );
  }

  renderItem(directory, i) {
    return (
      <section className="g-row g-bottom-space-narrow" key={i}>
        <section className="g-col-2">
          <CampaignQuickCreateDirectoryListSelect id={directory.get('id')} />
        </section>

        <section className="g-col-10">
          <h1 className="typo-no-vertical-rhythm">{directory.get('name')}</h1>
        </section>
      </section>
    );
  }
}
