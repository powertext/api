import React from 'react';
import { Link } from 'react-router';

export default class CampaignQuickCreateDirectoryListEmpty extends React.Component {
  render() {
    return (
      <div className="c-notice-section">
        <div className="innersection">
          <h1> No directories yet.. </h1>
          <Link to={'/directories/create'}>Why don't you create one?</Link>
        </div>
      </div>
    );
  }
}
