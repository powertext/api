import React, { PropTypes } from 'react';
import classnames from 'classnames';

import CampaignActions from '../../../actions/CampaignActions';
import CampaignQuickCreateDirectoryList from './CampaignQuickCreateDirectoryList';
import State from '../../../components/State';
import linkState from '../../../utils/linkState';
import * as semaphore from '../../../utils/semaphore';

export default class CampaignQuickCreate extends React.Component {
  static propTypes = {
    /**
     * @store DirectoryStore
     * @state directories
     */
    directories: PropTypes.object.isRequired,

    /**
     * DirectoryStore.getSelected
     */
    selectedDirectoryIds: PropTypes.arrayOf(
      PropTypes.number
    ).isRequired,

    /**
     * @store CampaignStore
     * @state isStoring
     */
    isStoring: PropTypes.bool.isRequired,

    /**
     * @store DirectoryStore
     * @state isFetchingAll
     */
    isDirectoryListFetching: PropTypes.bool.isRequired
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      message: '',
      header: ''
    };
  }

  render() {
    const { directories, isDirectoryListFetching, isStoring } = this.props;
    const { message } = this.state;

    return (
      <div className="g-col-6 c-panel">
        <h1> Quick Create Campaign </h1>
        <div className="typo-hr" />
        <h2> Select directories (recepients) </h2>
        <form onSubmit={::this._onSubmitHandle}>
          <CampaignQuickCreateDirectoryList
            directories={directories}
            isFetchingAll={isDirectoryListFetching} />

          <div className="typo-hr" />
          <section className="form-container">
            <label htmlFor="quick-create-campaign-header"> Header </label>
            <input
              type="text"
              className="form-control"
              id="quick-create-campaign-header"
              valueLink={linkState(this, 'header')} />
          </section>

          <section className="form-container">
            <label htmlFor="quick-create-message"> Message </label>
            <textarea
              rows="5"
              className="form-control"
              id="quick-create-message"
              valueLink={linkState(this, 'message')} />
          </section>

        <section className="g-bottom-space-narrow">
          <span className={classnames('c-label -small', {
            '-info': message.length < 50,
            '-warning': message.length > 50 && message.length < semaphore.CHARACTER_COUNT_PER_SMS,
            '-danger': message.length > semaphore.CHARACTER_COUNT_PER_SMS
          })}>
            <span className="emphasized"><i className="fa fa-font" /></span>
            {message.length}&#47;{semaphore.CHARACTER_COUNT_PER_SMS} <span className="lessemphasis">Characters</span>
          </span>

          <span className="c-label -info -small">
            <span className="emphasized"><i className="fa fa-mobile" /></span>
            {semaphore.price(message)} <span className="lessemphasis">PHP</span>
          </span>
        </section>

          <button className="c-btn -info" disabled={isStoring}>
            Create Campaign
          </button>
        </form>
      </div>
    );
  }

  _onSubmitHandle(evt) {
    evt.preventDefault();
    evt.stopPropagation && evt.stopPropagation();

    if ( this.props.isStoring ) {
      return;
    }

    CampaignActions.store({
      ...this.state,
      directory_ids: this.props.selectedDirectoryIds
    }).then(() => {
      alert('Successfully created the campaign');
      this.setState({ header: '', message: '' });
    }).catch(console.info);
  }
}
