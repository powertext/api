import AltContainer from 'alt/AltContainer';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Resolver } from 'react-resolver';

import ContactActions from '../../actions/ContactActions';
import ContactStore from '../../stores/ContactStore';
import State from '../../components/State';
import linkState from '../../utils/linkState';

class DirectoriesShowContactsShow extends React.Component {
  static displayName = 'DirectoriesShowContactsShow';

  static contextTypes = {
    /**
     * Router instance
     */
    router: PropTypes.object.isRequired
  };

  static propTypes = {
    // router params
    params: PropTypes.object.isRequired
  };

  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    const { contactId: id } = this.props.params;
    console.log('fetching..');
    ContactActions.fetch(id)
      .then(() => console.log('Fetching..'))
      .catch(() => console.log('Fetching..'));
  }

  render() {
    const { params: { directoryId, contactId } } = this.props;

    return <AltContainer
      store={ContactStore}
      render={({ contact }) => (
        <div>
          <div className="c-top-menu -no-allowance-top">
            <div className="left">
              <h1>Edit Contact</h1>
            </div>
          </div>

          <div className="g-wide">
            <State
              init={{
                number: contact.get('number'),
                first_name: contact.get('first_name'),
                last_name: contact.get('last_name')
              }}
              render={(state, ctx) => (
                <form onSubmit={::this._onSubmitHandle(state)}>
                  <div className="form-container">
                    <label htmlFor="new-contact-number">Mobile Number</label>
                    <input type="text"
                      id="new-contact-number"
                      name="number"
                      className="form-control"
                      valueLink={linkState(ctx, 'number')}
                      required />
                  </div>

                  <div className="form-container">
                    <label htmlFor="new-contact-fn">First Name</label>
                    <input type="text"
                      id="new-contact-fn"
                      name="first_name"
                      className="form-control"
                      valueLink={linkState(ctx, 'first_name')}
                      required />
                  </div>

                  <div className="form-container">
                    <label htmlFor="new-contact-ln">Last Name</label>
                    <input type="text"
                      id="new-contact-ln"
                      name="last_name"
                      className="form-control"
                      valueLink={linkState(ctx, 'last_name')}
                      required />
                  </div>

                  <div className="c-action-section">
                    <Link to={`/directories/${directoryId}/contacts/${contactId}`} className="left c-btn">Cancel</Link>
                    <button className="right c-btn -info">Update Contact</button>
                  </div>
                </form>
              )} />
          </div>
        </div>
      )} />
  }

  _onSubmitHandle(state) {
    return (evt) => {
      evt.preventDefault();
      evt.stopPropagation && evt.stopPropagation();
      const { directoryId, contactId } = this.props.params;

      ContactActions.update(directoryId, contactId, state)
        .then((res) => {
          alert('Successfully updated contact!');

          this.context.router
            .transitionTo(`/directories/${directoryId}`);
        });
    }
  }
}
export default DirectoriesShowContactsShow;
// export default Resolver.createContainer(DirectoriesShowContactsShow, {
//   resolve: {
//     // props
//     contact({ params }) {
//       const { contactId: id } = prams;
//       return ContactActions.fetch(id);
//     }
//   }
// });
