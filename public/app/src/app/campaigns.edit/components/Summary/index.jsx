import React, { PropTypes } from 'react';
import classnames from 'classnames';

import * as semaphore from '../../../../utils/semaphore';
import linkState from '../../../../utils/linkState';
import CampaignTermsModal from '../../../../components/CampaignTermsModal';

export default class Summary extends React.Component {
  static propTypes = {
    /**
     * Submit FN
     */
    onSubmitHandle: PropTypes.func.isRequired,

    /**
     * @store CampaignStore
     */
    isStoring: PropTypes.bool.isRequired,

    /**
     * @store CampaignStore
     */
    data: PropTypes.object.isRequired,

    /**
     * @param DirectoryStore
     */
    selectedDirectoryIds: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = { disclaimer: false };
  }

  render() {
    const { disclaimer } = this.state;
    const {
      data,
      onSubmitHandle,
      isStoring,
      selectedDirectoryIds
    } = this.props;


    return (
      <section>
        <div className="c-top-menu">
          <div className="left">
            <h1>Summary</h1>
          </div>
        </div>

        <section className="g-row g-bottom-space-narrow c-formatted-content">
          <section className="g-col-6">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </section>

          <section className="g-col-6">
            <h2><strong>{selectedDirectoryIds.size}</strong> Directories</h2>

            <span className={classnames('c-label -small', {
              '-info': data.message.length < 50,
              '-warning': data.message.length > 50 && data.message.length < semaphore.CHARACTER_COUNT_PER_SMS,
              '-danger': data.message.length > semaphore.CHARACTER_COUNT_PER_SMS
            })}>
              <span className="emphasized"><i className="fa fa-font" /></span>
              {data.message.length}&#47;{semaphore.CHARACTER_COUNT_PER_SMS} <span className="lessemphasis">Characters</span>
            </span>

            <span className="c-label -info -small">
              <span className="emphasized"><i className="fa fa-mobile" /></span>
              {semaphore.price(data.message)} <span className="lessemphasis">Credits</span>
            </span>
          </section>
        </section>

        <div className="form-container modal">
          <label>
            <input
              type="checkbox"
              name="disclaimer"
              checkedLink={linkState(this, 'disclaimer')} />
              <strong>You agree to the <span>terms and conditions</span> before sending this message.</strong>
          </label>
          <CampaignTermsModal />
        </div>

        <button onClick={onSubmitHandle}
          disabled={isStoring}
          className={classnames('c-btn -success', {
            '-hidden': !disclaimer
          })}>
          Send
        </button>
      </section>
    );
  }
}
