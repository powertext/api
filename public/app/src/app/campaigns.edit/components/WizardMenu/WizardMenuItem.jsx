import React from 'react';
import classnames from 'classnames';

export default class WizardMenuItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { active, children, jumpStepHandle } = this.props;

    return (
      <a className={classnames('step', { '-active': active })}
        href="javascript:void(0);"
        onClick={jumpStepHandle}>
        {children}
      </a>
    );
  }
}
