import React from 'react';
import WizardMenuItem from './WizardMenuItem';

export default class WizardMenu extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { active, jumpStepHandle } = this.props;
    return (
      <div className="c-wizard-footer -fixed">
        <div className="steps">
          <WizardMenuItem
            active={active === 0}
            jumpStepHandle={jumpStepHandle.bind(null, 0)}>
            Select Directories
          </WizardMenuItem>
          <WizardMenuItem
            active={active === 1}
            jumpStepHandle={jumpStepHandle.bind(null, 1)}>
            Compose Message
          </WizardMenuItem>
          <WizardMenuItem
            active={active === 2}
            jumpStepHandle={jumpStepHandle.bind(null, 2)}>
            Summary
          </WizardMenuItem>
        </div>

        <div className="actions">{
          active < 2
            ? (<button
                type="button"
                className="c-btn -info -top"
                onClick={jumpStepHandle.bind(null, active + 1)}>
                Next
              </button>
              )
            : null
        }</div>
      </div>

    );
  }
}
