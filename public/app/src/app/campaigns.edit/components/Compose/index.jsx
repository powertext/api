import React, { PropTypes } from 'react';
import classnames from 'classnames';

import CampaignsMessageCharacterCountLabel from '../../../../components/CampaignsMessageCharacterCountLabel';
import CampaignsMessagePrice from '../../../../components/CampaignsMessagePrice';

export default class Compose extends React.Component {
  static propTypes = {
    /**
     * valueLink binded to the container
     */
    linkState: PropTypes.func.isRequired,

    /**
     * Container state
     */
    data: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { data, linkState } = this.props;

    return (
      <section>
        <h1>Compose Message</h1>

        <div className="form-container g-wide">
          <section className="form-container">
            <label htmlFor="campaign-create-name"> Name </label>
            <input
              type="text"
              className="form-control"
              id="campaign-create-name"
              valueLink={linkState('name')} />
          </section>

          <section className="form-container">
            <label htmlFor="campaign-create-message"> Message </label>
            <textarea
              id="campaign-create-message"
              placeholder="Enter your message here.."
              rows="5"
              name="message"
              className="form-control"
              onKeyUp={::this.changeMessageHandler}
              valueLink={linkState('message')} />
          </section>
        </div>

        <section className="g-bottom-space-narrow">
          <CampaignsMessageCharacterCountLabel message={data.message} />
          <CampaignsMessagePrice message={data.message} />
        </section>
      </section>
    );
  }

  changeMessageHandler(evt) {
    console.log(linkState('message')); 
  }
}
