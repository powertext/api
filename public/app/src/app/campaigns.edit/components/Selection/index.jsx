import React, { PropTypes } from 'react';

import DirectoryStore from '../../../../stores/DirectoryStore';
import Item from './Item';

export default class Selection extends React.Component {
  static propTypes = {
    /**
     * @store DirectoryStore
     */
    directories: PropTypes.object.isRequired,

    /**
     * @store DirectoryStore
     */
    isFetchingAll: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { directories, isFetchingAll } = this.props;

    return (
      <section>
        <div className="c-top-menu">
          <div className="left">
            <h1>To which directory will we send?</h1>
          </div>
        </div>

        <div className="c-selection-list">
        { !isFetchingAll
          ? directories.map((directory, i) => {
              return <Item directory={directory} key={i} />;
            })
          : null
        }
        </div>
      </section>
    );
  }
}
