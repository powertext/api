import React, { PropTypes } from 'react';
import classnames from 'classnames';

import ItemSelect from './ItemSelect';

export default class SelectionItem extends React.Component {
  static propTypes = {
    /**
     * @store DirectoryStore.x
     */
    directory: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      expanded: false
    };
  }

  render() {
    const { expanded } = this.state;
    const { directory } = this.props;

    return (
      <div className={classnames('item', {
        '-expanded': expanded,
        '-closed': !expanded
      })}>
        <section className="g-row">
          <section className="g-col-8 g-row">
            <section className="g-col-1">
              <ItemSelect id={directory.get('id')} />
            </section>
            <section className="g-col-11">
              <h2>{directory.get('directory_name')}</h2>
              <h3>(25 Contacts)</h3>
            </section>
          </section>
          <section className="g-col-4">
            <div className="actions">
              <button type="button" className="c-btn" onClick={::this._toggleExpand}>
                <i className={classnames('fa', {
                  'fa-caret-right': !expanded,
                  'fa-caret-down': expanded
                })} />
              </button>
            </div>
          </section>
        </section>

        <div className="c-selection-list">
          <div className="item">
            <section className="g-row">
              <section className="g-col-8 g-row">
                <label className="form-checkbox g-col-1">
                  <input type="checkbox" />
                  <div></div>
                </label>
                <section className="g-col-11">
                  <h3>Rico Santos</h3>
                </section>
              </section>
            </section>
          </div>

          <div className="item">
            <section className="g-row">
              <section className="g-col-8 g-row">
                <label className="form-checkbox g-col-1">
                  <input type="checkbox" />
                  <div></div>
                </label>
                <section className="g-col-11">
                  <h3>John Doe</h3>
                </section>
              </section>
            </section>
          </div>
        </div>
      </div>
    );
  }

  /**
   * Toggle
   */
  _toggleExpand(status) {
    this.setState({
      expanded: !this.state.expanded
    });
  }
}
