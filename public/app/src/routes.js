import React from 'react';
import { Router, Route } from 'react-router';
import HashHistory from 'react-router/lib/HashHistory';
//import createBrowserHistory from 'history/lib/createBrowserHistory'

import App from './app/App';
import Main from './app/_main';
import Home from './app/home';
import Directories from './app/directories';
import DirectoriesIndex from './app/directories.index';
import DirectoriesCreate from './app/directories.create';
import DirectoriesShow from './app/directories.show';
// import DirectoriesShowContacts from './app/directories.show.contacts';
import DirectoriesShowContactsCreate from './app/directories.show.contacts-create';
import DirectoriesShowContactsShow from './app/directories.show.contacts-show';
import DirectoriesShowContactsImport from './app/directories.show.contacts-import';
import DirectoriesShowContactsHistory from './app/directories.show.contacts-show.history';
import DirectoriesShowSettings from './app/directories.show.settings';
import CampaignsIndex from './app/campaigns.index';
import CampaignsCreate from './app/campaigns.create';
import CampaignsShow from './app/campaigns.show';
import CampaignsEdit from './app/campaigns.edit';
import ReportsIndex from './app/reports.index';
import AuthForgotPassword from './app/auth.forgot-pass';
import AuthLogin from './app/auth.login';

export default (
  <Router history={new HashHistory()}>
    <Route component={App}>
      <Route path="login" component={AuthLogin} />
      <Route path="forgot-password" component={AuthForgotPassword} />

      <Route component={Main}>
        <Route path="/" component={Home} />

        <Route path="directories" component={DirectoriesIndex} />
        <Route path="directories/create" component={DirectoriesCreate} />

        <Route path="directories/:directoryId" component={DirectoriesShow}>
          <Route path="settings" component={DirectoriesShowSettings} />
          <Route path="contacts/add" component={DirectoriesShowContactsCreate} />
          <Route path="contacts/import" component={DirectoriesShowContactsImport} />
          <Route path="contacts/:contactId" component={DirectoriesShowContactsShow} />
          <Route path="contacts/:contactId/history" component={DirectoriesShowContactsHistory} />
        </Route>

        <Route path="campaigns" component={CampaignsIndex} />
        <Route path="campaigns/create" component={CampaignsCreate} />
        <Route path="campaigns/:campaignId" component={CampaignsShow} />
        <Route path="campaigns/:campaignId/edit" component={CampaignsEdit} />

        <Route path="reports" component={ReportsIndex} />
      </Route>
    </Route>
  </Router>
);
