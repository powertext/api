import { createStore } from 'alt/utils/decorators';
import Immutable from 'immutable';
import immutable from 'alt/utils/ImmutableUtil';

import alt from '../alt';
import ContactActions from '../actions/ContactActions';


@createStore(alt)
@immutable
class SelectedContactStore {
  state = {
    selected: Immutable.List()
  };

  constructor() {
    this.bindListeners({
      // onFetchAll: onFetchAll,
      onSelectContact: ContactActions.SELECT_CONTACT,
      onSelectDirectory: ContactActions.SELECT_DIRECTORY
    });

    this.exportPublicMethods({
      getIndexById: ::this.getIndexById,
      isContactSelected: ::this.isContactSelected,
      isDirectorySelected: ::this.isDirectorySelected
    });
  }

  isDirectorySelected(directory) {
    const { selected } = this.state;
    const contacts = directory.get('contacts');


    console.log(contacts, selected.toJS());

    return !contacts.size ? false : contacts
      .filter(contact => selected.indexOf(contact.get('id')) !== -1)
        .size === contacts.size;
  }

  isContactSelected(id) {
     return this.getIndexById(id) !== -1;
  }

  getIndexById(id) {
    return this.state.selected.indexOf(id);
  }

  // onFetchAll() {
  //   this.waitFor(DirectoryStore);

  //   const ids = DirectoryStore.getState().directories
  //     .map(directory => directory.get('contacts'))
  //       .filter(contact => directory.get(SELECTED))
  //         .map(directory => directory.get('id'));

  //   this.setState({ selected: this.state.selected.merge(ids) });
  // }

  onSelectContact(id) {
    this.setState({
      selected: this.state.selected.push(id)
    });
  }

  onSelectDirectory(directory) {
    const contacts = directory.get('contacts');
    const { selected } = this.state;

    this.setState({
      selected: !this.isDirectorySelected(directory) ? selected.concat(
        contacts.filter(contact => (
          selected.indexOf(contact.get('id')) === -1
        )).map(contact => contact.get('id'))
      ) : selected.filter(id => (
        contacts.map(contact => contact.get('id')).indexOf(id) === -1
      ))
    });
  }
}

export default SelectedContactStore;
