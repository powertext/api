import Immutable from 'immutable';
import immutable from 'alt/utils/ImmutableUtil';
import { createStore } from 'alt/utils/decorators';

import alt from '../alt';
import CampaignActions from '../actions/CampaignActions';

const SELECTED = '__SELECTED__';
const SORT = '_SORT_';
const SORT_ASCENDING = '_SORT_ASCENDING_';
const SORT_DESCENDING = '_SORT_DESCENDING_';

@createStore(alt)
@immutable
class CampaignStore {
  static displayName = 'CampaignStore';

  state = {
    sort: SORT,
    campaigns: Immutable.List(),
    campaign: Immutable.Map(),
    isFetchingAll: false,
    isFetchingAllError: false,
    isFetching: false,
    isFetchingError: false,
    isStoring: false,
    isStoringError: false,
    isUpdating: false,
    isUpdatingError: false
  };

  constructor() {
    this.bindActions(CampaignActions);

    this.exportPublicMethods({
      getIndexById: ::this.getIndexById,
      isAllSelected: ::this.isAllSelected,
      isAnySelected: ::this.isAnySelected,
      isSelected: ::this.isSelected,
      isSortAscending: ::this.isSortAscending,
      isSortDescending: ::this.isSortDescending
    });
  }

  // helper
  getIndexById(id) {
    return this.state.campaigns
      .findIndex(d => d.get('id') === id);
  }

  // helper
  isAllSelected() {
    const { campaigns } = this.state;

    return campaigns.size === campaigns
      .filter(d => d.get(SELECTED))
        .size;
  }

  // helper
  isSelected(id) {
    return this.state.campaigns
      .get(this.getIndexById(id))
        .get(SELECTED);
  }

  // helper
  // inverse of
  // isSelectedEmpty
  isAnySelected() {
    return !!(this.state.campaigns
      .filter(campaign => campaign.get(SELECTED))
        .size);
  }

  isSortAscending() {
    return this.state.sort === SORT_ASCENDING;
  }

  isSortDescending() {
    return this.state.sort === SORT_DESCENDING;
  }

  onFetchAll() {
    this.setState({
      ...this.state,
      isFetchingAll: true,
      isFetchingAllError: false
    });
  }

   onFetchAllSuccess(campaigns) {
    this.setState({
      ...this.state,
      campaigns: Immutable.fromJS(campaigns),
      isFetchingAll: false,
      isFetchingAllError: false
    });
  }

  onFetchAllError() {
    this.setState({
      ...this.state,
      isFetchingAll: false,
      isFetchingAllError: true
    });
  }

  onFetch() {
    this.setState({
      ...this.state,
      campaigns: this.state.campaigns,
      campaign: this.state.campaign,
      isFetching: true,
      isFetchingError: false
    });
  }

  onFetchSuccess(campaign) {
    const map = Immutable.Map(campaign);

    this.setState({
      ...this.state,
      campaigns: this.state.campaigns.push(map),
      campaign: map,
      isFetching: false,
      isFetchingError: false
    });
  }

  onFetchError(err) {
    this.setState({
      ...this.state,
      isFetching: false,
      isFetchingError: true
    });
  }

  onStore() {
    this.setState({
      ...this.state,
      campaigns: this.state.campaigns,
      isStoring: true,
      isStoringError: false
    });
  }

  onStoreSuccess(campaign) {
    const map = Immutable.Map(campaign);

    this.setState({
      ...this.state,
      campaigns: this.state.campaigns.push(map),
      campaign: campaign,
      isStoring: false,
      isStoringError: false
    });
  }

  onStoreError(err) {
    this.setState({
      ...this.state,
      isStoring: false,
      isStoringError: true
    });
  }

  onSelect(id) {
    const campaigns = this.state.campaigns
      .update(
        this.getIndexById(id),
        campaign => campaign.set(SELECTED, !this.isSelected(id))
      );

    this.setState({
      ...this.state,
      campaigns
    });
  }

  onSelectAll() {
    this.setState({
      ...this.state,
      campaigns: this.state.campaigns
        .map(campaign => campaign.set(SELECTED, !this.isAllSelected()))
    });
  }

  onDeleteSelected() {
    this.setState({
      ...this.state,
      campaigns: this.state.campaigns
        .filter(campaign => !campaign.get(SELECTED))
    });
  }

  onSend(campaign) {
    this.setState({
      ...this.state,
      campaigns: this.state.campaigns,
      campaign: this.state.campaign,
      isSending: true,
      isSendingError: false
    });
  }

  onSendSuccess(campaign) {
    const map = Immutable.Map(campaign);
    const index = this.getIndexById(map.get('id'));
    const campaigns = this.state.campaigns;

    this.setState({
      ...this.state,
      // Needs refactoring for paginated data
      campaigns: index === -1
        ? campaigns
        : campaigns.update(index, campaign => map),
      campaign: map,
      isSending: false,
      isSendingError: false
    });
  }

  onSendError(err) {
    this.setState({
      ...this.state,
      campaigns: this.state.campaigns,
      campaign: this.state.campaign,
      isSending: false,
      isSendingError: true
    });
  }

  onSort() {
    let sort;

    switch(this.state.sort) {
      case SORT: sort = SORT_ASCENDING; break;
      case SORT_ASCENDING: sort = SORT_DESCENDING; break;
      case SORT_DESCENDING:
      default: sort = SORT; break;
    }

    this.setState({
      ...this.state,
      sort
    });
  }
}

export default CampaignStore;
