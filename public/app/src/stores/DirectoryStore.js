import Immutable from 'immutable';
import immutable from 'alt/utils/ImmutableUtil';
import { createStore } from 'alt/utils/decorators';

import alt from '../alt';
import DirectoryActions from '../actions/DirectoryActions';
import CampaignActions from '../actions/CampaignActions';

const SELECTED = '_SELECTED_';
const SORT = '_SORT_';
const SORT_ASCENDING = '_SORT_ASCENDING_';
const SORT_DESCENDING = '_SORT_DESCENDING_';

@createStore(alt)
@immutable
class DirectoryStore {
  static displayName = 'DirectoryStore';

  state = {
    sort: SORT,
    directories: Immutable.List(),
    directory: Immutable.Map(),
    isFetchingAll: false,
    isFetchingAllError: false,
    isFetching: false,
    isFetchingError: false,
    isStoring: false,
    isStoringError: false,
    isUpdating: false,
    isUpdatingError: false
  };

  constructor() {
    this.bindListeners({
      onSort: DirectoryActions.SORT,

      onFetchAll: DirectoryActions.FETCH_ALL,
      onFetchAllSuccess: DirectoryActions.FETCH_ALL_SUCCESS,
      onFetchAllError: DirectoryActions.FETCH_ALL_ERROR,

      onFetch: DirectoryActions.FETCH,
      onFetchSuccess: DirectoryActions.FETCH_SUCCESS,
      onFetchError: DirectoryActions.FETCH_ERROR,

      onStore: DirectoryActions.STORE,
      onStoreSuccess: DirectoryActions.STORE_SUCCESS,
      onStoreError: DirectoryActions.STORE_ERROR,

      onUpdate: DirectoryActions.UPDATE,
      onUpdateSuccess: DirectoryActions.UPDATE_SUCCESS,
      onUpdateError: DirectoryActions.UPDATE_ERROR,

      onDelete: DirectoryActions.DELETE,

      onSelect: DirectoryActions.SELECT,
      onSelectAll: DirectoryActions.SELECT_ALL,
      onDeleteSelected: DirectoryActions.DELETE_SELECTED,

      onCampaignStoreSuccess: CampaignActions.STORE_SUCCESS
    });

    this.exportPublicMethods({
      getIndexById: ::this.getIndexById,
      isAllSelected: ::this.isAllSelected,
      isAnySelected: ::this.isAnySelected,
      isSelected: ::this.isSelected,
      getSelected: ::this.getSelected,
      getSelectedIds: ::this.getSelectedIds,
      isSortAscending: ::this.isSortAscending,
      isSortDescending: ::this.isSortDescending
    });
  }

  // helper
  getIndexById(id) {
    return this.state.directories
      .findIndex(d => d.get('id') === id);
  }

  // helper
  isAllSelected() {
    const { directories } = this.state;

    return directories.size === directories
      .filter(d => d.get(SELECTED))
        .size;
  }

  // helper
  isSelected(id) {
    return this.state.directories
      .get(this.getIndexById(id))
        .get(SELECTED);
  }

  // helper
  // inverse of
  // isSelectedEmpty
  isAnySelected() {
    return !!(this.getSelected().size);
  }

  //helper
  getSelected() {
    return this.state.directories
      .filter(directory => directory.get(SELECTED));
  }

  //helper
  getSelectedIds() {
    return this.getSelected()
      .map(directory => directory.get('id'));
  }

  isSortAscending() {
    return this.state.sort === SORT_ASCENDING;
  }

  isSortDescending() {
    return this.state.sort === SORT_DESCENDING;
  }

  onFetchAll() {
    this.setState({
      ...this.state,
      directories: Immutable.List(),
      isFetchingAll: true,
      isFetchingAllError: false
    });
  }

  onFetchAllSuccess(directories) {
    this.setState({
      ...this.state,
      directories: Immutable.fromJS(directories),
      isFetchingAll: false,
      isFetchingAllError: false
    });
  }

  onFetchAllError(err) {
    this.setState({
      ...this.state,
      isFetchingAll: false,
      isFetchingAllError: true
    });
  }

  onFetch() {
    this.setState({
      ...this.state,
      // ito work around lang talaga. promise. crosses my heartses.
      // parang natatanggal yung `state.directory` everytime na
      // mag-setState ka tapos hindi included yung property na yun.
      // e.g.,
      // setState({ ratbu: 'ka', pogi: 'ako' }); => { ratbu, pogi }
      // setState({ ratbu: 'kayo' }) => { ratbu }
      // WOW DIBA?
      directory: Immutable.Map(),

      isFetching: true,
      isFetchingError: false
    });
  }

  onFetchSuccess(directory) {
    this.setState({
      ...this.state,
      directory: Immutable.Map(directory),
      isFetchingError: false,
      isFetching: false
    });
  }

  onFetchError() {
    this.setState({
      ...this.state,
      isFetchingError: true,
      isFetching: false
    });
  }

  onStore() {
    this.setState({
      ...this.state,
      directories: this.state.directories,
      isStoring: true,
      isStoringError: false
    });
  }

  onStoreSuccess(directory) {
    const map = Immutable.Map(directory);

    this.setState({
      ...this.state,
      directories: this.state.directories.push(map),
      isStoring: false,
      isStoringError: false
    });
  }

  onStoreError() {
    this.setState({
      ...this.state,
      isStoring: false,
      isStoringError: true
    });
  }

  onUpdate() {
    this.setState({
      ...this.state,
      isUpdating: true,
      isUpdatingError: false
    });
  }

  onUpdateSuccess(directory) {
    this.setState({
      ...this.state,
      directory: Immutable.Map(directory),
      isUpdating: false,
      isUpdatingError: false
    });
  }

  onUpdateError(err) {
    this.setState({
      ...this.state,
      isUpdating: false,
      isUpdatingError: true
    });
  }

  onDelete() {
    //
  }

  onSelect(id) {
    const directories = this.state.directories
      .update(
        this.getIndexById(id),
        d => d.set(SELECTED, !this.isSelected(id))
      );

    this.setState({ directories });
  }

  onSelectAll() {
    this.setState({
      ...this.state,
      directories: this.state.directories
        .map(d => d.set(SELECTED, !this.isAllSelected()))
    });
  }

  onDeleteSelected() {
    this.setState({
      ...this.state,
      directories: this.state.directories
        .filter(d => !d.get(SELECTED))
    });
  }

  onCampaignStoreSuccess(campaign) {
    this.setState({
      ...this.state,
      directories: this.state.directories
        .map(directory => directory.set(SELECTED, false))
          .push(Immutable.Map(campaign))
    })
  }

  onSort() {
    let sort;

    switch(this.state.sort) {
      case SORT: sort = SORT_ASCENDING; break;
      case SORT_ASCENDING: sort = SORT_DESCENDING; break;
      case SORT_DESCENDING:
      default: sort = SORT; break;
    }

    this.setState({
      ...this.state,
      sort
    });
  }
}

export default DirectoryStore;
