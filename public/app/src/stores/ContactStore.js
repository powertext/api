import alt from '../alt';
import { createStore } from 'alt/utils/decorators';
import immutable from 'alt/utils/ImmutableUtil';
import Immutable from 'immutable';

import ContactActions from '../actions/ContactActions';

const SELECTED = '_SELECTED_';
const SORT = '_SORT_';
const SORT_ASCENDING = '_SORT_ASCENDING_';
const SORT_DESCENDING = '_SORT_DESCENDING_';

@createStore(alt)
@immutable
class ContactStore {
  static displayName = 'ContactStore';

  state = {
    sort: SORT,
    contacts: Immutable.List(),
    contact: Immutable.Map(),
    isFetchingAll: false,
    isFetchingErrorAll: false,
    isFetching: false,
    isFetchingError: false,
    isStoring: false,
    isStoringError: false,
    isUpdating: false,
    isUpdatingError: false,
  };

  constructor() {
    this.bindListeners({
      onSort: ContactActions.SORT,
      onFetchAll: ContactActions.FETCH_ALL,
      onFetchAllSuccess: ContactActions.FETCH_ALL_SUCCESS,
      onFetchAllError: ContactActions.FETCH_ALL_ERROR,
      onFetch: ContactActions.FETCH,
      onFetchSuccess: ContactActions.FETCH_SUCCESS,
      onFetchError: ContactActions.FETCH_ERROR,
      onStore: ContactActions.STORE,
      onStoreSuccess: ContactActions.STORE_SUCCESS,
      onStoreError: ContactActions.STORE_ERROR,
      onUpdate: ContactActions.UPDATE,
      onUpdateSuccess: ContactActions.UPDATE_SUCCESS,
      onUpdateError: ContactActions.UPDATE_ERROR,
      onSelect: ContactActions.SELECT,
      onSelectAll: ContactActions.SELECT_ALL,
      onDeleteSelected: ContactActions.DELETE_SELECTED
    });

    this.exportPublicMethods({
      getIndexById: ::this.getIndexById,
      isAllSelected: ::this.isAllSelected,
      isAnySelected: ::this.isAnySelected,
      isSelected: ::this.isSelected,
      getSelected: ::this.getSelected,
      getSelectedIds: ::this.getSelectedIds,
      isSortAscending: ::this.isSortAscending,
      isSortDescending: ::this.isSortDescending
    });
  }

  // helper
  getIndexById(id) {
    return this.state.contacts
      .findIndex(c => c.get('id') === id);
  }

  // helper
  isAllSelected() {
    const { contacts } = this.state;

    return contacts.size === contacts
      .filter(d => d.get(SELECTED))
        .size;
  }

  // helper
  isSelected(id) {
    return this.state.contacts
      .get(this.getIndexById(id))
        .get(SELECTED);
  }

  // helper
  // inverse of
  // isSelectedEmpty
  isAnySelected() {
    return !!(this.getSelected().size);
  }

  //helper
  getSelected() {
    return this.state.contacts
      .filter(contact => contact.get(SELECTED));
  }

  //helper
  getSelectedIds() {
    return this.getSelected()
      .map(contact => contact.get('id'));
  }

  isSortAscending() {
    return this.state.sort === SORT_ASCENDING;
  }

  isSortDescending() {
    return this.state.sort === SORT_DESCENDING;
  }

  onFetchAll() {
    this.setState({
      ...this.state,
      contacts: Immutable.List(),
      isFetchingAll: true,
      isFetchingAllError: false
    });
  }

  onFetchAllSuccess(contacts) {
    this.setState({
      ...this.state,
      contacts: Immutable.fromJS(contacts),
      isFetchingAll: false,
      isFetchingAllError: false
    });
  }

  onFetchAllError(err) {
    this.setState({
      ...this.state,
      isFetchingAll: false,
      isFetchingAllError: true
    });
  }

  onFetch() {
    this.setState({
      ...this.state,
      contacts: this.state.contacts,
      isFetchingAll: true,
      isFetchingAllError: false
    });
  }

  onFetchSuccess(contact) {
    const map = Immutable.Map(contact);

    this.setState({
      ...this.state,
      contacts: this.state.contacts.push(map),
      contact: map,
      isFetching: false,
      isFetchingError: false
    });
  }

  onFetchError(err) {
    this.setState({
      ...this.state,
      isFetching: false,
      isFetchingError: true
    });
  }

  onStore() {
    this.setState({
      ...this.state,
      isStoring: true,
      isStoringError: false
    });
  }

  onStoreSuccess(contact) {
    const map = Immutable.Map(contact);

    this.setState({
      ...this.state,
      contacts: this.state.contacts.push(map),
      contact: map,
      isStoring: false,
      isStoringError: false
    });
  }

  onStoreError(err) {
    this.setState({
      ...this.state,
      isStoring: false,
      isStoringError: false
    });
  }

  onUpdate(payload) {
    this.setState({
      ...this.state,
      contacts: this.state.contacts,
      contact: this.state.contact,
      isUpdating: true,
      isUpdatingError: false
    });
  }

  onUpdateSuccess(contact) {
    const map = Immutable.Map(contact);
    const index = this.getIndexById(contact.get('id'));
    const { contacts } = this.state;

    this.setState({
      ...this.state,
      contacts: index !== -1
        ? contacts.update(index, contact => map)
        : contacts,
      contact: map,
      isUpdating: false,
      isUpdatingError: false
    });
  }

  onUpdateError(err) {
    this.setState({
      ...this.state,
      contacts: this.state.contacts,
      contact: this.state.contact,
      isUpdating: false,
      isUpdatingError: true
    });
  }

  onDeleteSelected() {
    this.setState({
      ...this.state,
      contacts: this.state.contacts
        .filter(contact => !contact.get(SELECTED))
    });
  }

  onSelect(id) {
    this.setState({
      ...this.state,
      contacts: this.state.contacts.update(
        this.getIndexById(id),
        contact => contact.set(SELECTED, !this.isSelected(id))
      )
    });
  }

  onSelectAll() {
    const isAllSelected = !this.isAllSelected();

    this.setState({
      ...this.state,
      contacts: this.state.contacts.map(
        contact => contact.set(SELECTED, isAllSelected)
      )
    });
  }

  onSort() {
    let sort;

    switch(this.state.sort) {
      case SORT: sort = SORT_ASCENDING; break;
      case SORT_ASCENDING: sort = SORT_DESCENDING; break;
      case SORT_DESCENDING:
      default: sort = SORT; break;
    }

    this.setState({
      ...this.state,
      sort
    });
  }
}

export default ContactStore;
