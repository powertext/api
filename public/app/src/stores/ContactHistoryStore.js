import { createStore } from 'alt/utils/decorators';
import immutable from 'alt/utils/ImmutableUtil';
import Immutable from 'immutable';

import alt from '../alt';
import ContactHistoryActions from '../actions/ContactHistoryActions';

@createStore(alt)
@immutable
class ContactHistoryStore {
  static displayName = 'ContactHistoryStore';

  state = {
    messages: Immutable.List(),
    isFetchingAll: false,
    isFetchingAllError: false
  };

  constructor() {
    this.bindActions(ContactHistoryActions);
  }

  onFetchAll() {
    this.setState({
      ...this.state,
      isFetchingAll: true,
      isFetchingAllError: false
    });
  }

  onFetchAllSuccess(messages) {
    this.setState({
      ...this.state,
      messages: Immutable.fromJS(messages),
      isFetchingAll: false
    });
  }

  onFetchAllError() {
    this.setState({
      ...this.state,
      isFetchingAll: false,
      isFetchingAllError: true
    });
  }
}

export default ContactHistoryStore;
