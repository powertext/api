import Immutable from 'immutable';

import alt from '../alt';
import immutable from 'alt/utils/ImmutableUtil';
import SessionActions from '../actions/SessionActions';
import config from '../config';
import { createStore } from 'alt/utils/decorators';

@createStore(alt)
@immutable
export default class SessionStore {
  state = {
    auth: null,
    authLoading: false
  };

  cb = null;

  constructor() {
    this.bindActions(SessionActions);
    this.bindListeners({
      onAuthCheckSuccess: SessionActions.GET_SUCCESS,
      onAuthCheckError: SessionActions.GET_ERROR,
      onLoginSuccess: SessionActions.LOGIN_SUCCESS,
      onLoginError: SessionActions.LOGIN_ERROR
      //nAuthSuccess: SessionActions.AUTH_SUCCESS,
      //onAuthError: SessionActions.AUTH_ERROR
    });

    this.exportPublicMethods({
        onChange: ::this.onChange,
        logout: ::this.logout, 
    });
  }

  logout() {
      localStorage.removeItem(config.AUTH_KEY);
      this.setState({
          auth: null
      });
  }

  onAuthCheckSuccess(data) {
   localStorage.getItem(config.AUTH_KEY);
   this.setState({
      auth:data
   });
  }

  onAuthCheckError(error) {
   console.log(error); 
   this.setState({
	    auth: null
    });
   //this.cb(this.getState());
  }

  onLoginSuccess(data) {
      localStorage.setItem(config.AUTH_KEY, data.token);
      this.setState({
          auth: data
      });
      this.cb(data);
  }

  onLoginError(error) {
      console.warn(error);
  
      this.setState({
          auth: null
      });
      this.cb("an error has occured");
  }

  onChange(cb) {
      this.cb = cb;
  }



 // onAuthAttempt() {
 //   this.setState({
 //     authLoading: jwt.decode(cookie.get(config.AUTH_KEY));
 //   });
 //  }

  // onAuthSuccess() {
  //   this.setState({
  //     auth: jwt.decode(cookie.get(config.AUTH_KEY));
  //   });
  // }

  // onAuthError() {
  //   this.setState({
  //     auth: jwt.decode(cookie.get(config.AUTH_KEY));
  //   });
  // }
}
