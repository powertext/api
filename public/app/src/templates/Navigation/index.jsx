import React from 'react';
import { Link } from 'react-router';
import NavDropDownAccount from '../../components/NavDropDownAccount';

export default class Navigation extends React.Component {

    static propTypes = {
        auth: React.PropTypes.object
    };

    constructor(props, context) {
        super(props, context);
    }
  
 render() {
     var { auth } = this.props; 
    return (
      <div className="c-nav">
        <div className="left">
          <Link className="nav-bar" to='{/}'>POWERTEXT</Link>
          <Link activeClassName="-active" > Home </Link>
          <Link activeClassName="-active" to={'/directories'}> Directories </Link>
          <Link activeClassName="-active" to={'/campaigns'}> Campaigns </Link>
          <Link activeClassName="-active" to={'/reports'}> Reports </Link>
        </div>

        <div className="right">
            <NavDropDownAccount auth={auth}  />
        </div>
      </div>
    );
  }
}
