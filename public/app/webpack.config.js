var webpack = require('webpack');
var parse = JSON.stringify;
var __PRODUCTION__ = process.env.NODE_ENV === 'production';

module.exports = {
  entry: './src/app.js',
  output: {
    filename: 'script.js', // Filename of the output
    path: 'dist/' // Directory of the output
  },
  // watch: true, // change for prod pls
  module: {
    // Transformers
    loaders: [
      // Transform to es6 with babel
      // http://webpack.github.io/docs/loaders.html
      { test: /\.(js|jsx|es6)$/, exclude: 'node_modules/', loader: 'babel-loader?modules=common&stage=0' }
    ]
  },
  resolve: {
    // So we can require files without specfiying the file extension.
    // e.g., require('./yolo.es6') => require('./yolo');
    extensions: ['', '.js', '.json', '.jsx', '.es6']
  },
  plugins: [
    new webpack.DefinePlugin({
      __PRODUCTION__: parse(__PRODUCTION__),
      __API__: parse(
        (__PRODUCTION__ ? '//edisontan.cloudapp.net' : process.env.API || '//powertext.app')
        + '/api'
      )
    })
  ]
};
