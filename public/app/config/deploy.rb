set :stages, %w(production staging)
set :default_stage, "staging"
set :application, "one-express"
set :repo_url, "git@gitlab.com:powertext/web-new.git"
set :scm, :git

set :user, "ec2-user"

set :branch, "master"

set :ssh_options, {:auth_methods => "publickey"}
set :ssh_options, {:keys => ["~/Documents/realtydig.pem"]}

set :deploy_to, "/var/www/powertext-web"

set :keep_releases, 3

desc "check production task"
task :check_production do

end


